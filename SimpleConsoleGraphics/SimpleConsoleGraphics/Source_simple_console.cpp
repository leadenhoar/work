#include <windows.h>
#include <math.h>
#include <iostream>

#define PI 3.1415926

using namespace std;

void points();
void axis(int x, int y, int lx, int ly);
void print_line(int x1, int y1, int x2, int y2);

int function(int x)
{
	return 200 * sin((double)x/100 * PI);
}

int main()
{
	//Ellipse(hDC, 12, 23, 130, 130);					//��� ��� �����, �� �� ����������
	//points();
	int cx = 320,
		cy = 250,
		lx = 500,
		ly = 400;
	axis(cx, cy, lx, ly);

	int prevx = -lx / 2, prevy = function(prevx);
	for (int x = -lx / 2 + 1; x < lx / 2; ++x)
	{
		int y = function(x);
		print_line(cx + prevx, cy + prevy, cx + x, cy + y);
		prevx = x;
		prevy = y;
		
	}

	system("pause");
	return 0;
}



void axis(int x, int y, int lx, int ly)
{
	POINT op;
	HWND hWnd = GetConsoleWindow();					//�������� ���������� ���� �������
	HDC hDC = GetDC(hWnd);							//�������� �������� ���������� �� ����������� �����������
	SelectObject(hDC, GetStockObject(WHITE_PEN));	//�������� ���� WHITE_PEN � ��������
	
	print_line((x - lx / 2), y, (x + lx / 2 + 1), y);

	print_line(x, (y - ly / 2), x, (y + ly / 2 + 1));		

	ReleaseDC(hWnd, hDC);							//"�����������" ��������
	
}


void print_line(int x1, int y1, int x2, int y2)
{
	POINT op;
	HWND hWnd = GetConsoleWindow();					//�������� ���������� ���� �������
	HDC hDC = GetDC(hWnd);							//�������� �������� ���������� �� ����������� �����������
	SelectObject(hDC, GetStockObject(WHITE_PEN));	//�������� ���� WHITE_PEN � ��������

	MoveToEx(hDC, x1, y1, &op);
	LineTo(hDC, x2, y2);

	ReleaseDC(hWnd, hDC);							//"�����������" ��������	
}

void points()
{
	POINT op;
	HWND hWnd = GetConsoleWindow();					//�������� ���������� ���� �������
	HDC hDC = GetDC(hWnd);							//�������� �������� ���������� �� ����������� �����������
	SelectObject(hDC, GetStockObject(WHITE_PEN));	//�������� ���� WHITE_PEN � ��������

	cout << "Set points count:" << endl;
	size_t count = 0;
	cin >> count;
	cout << "Set points coord:" << endl;

	for (size_t i = 0; i < count; ++i)
	{
		int x = 0, y = 0;
		cin >> x >> y;
		
		if (!i)
		{
			MoveToEx(hDC, x, y, &op);				//������ ������ ����� � (x, y)
		}
		else
		{
			LineTo(hDC, x, y);						//������ ����� �� ������� ����� � (x, y)
		}
	}

	ReleaseDC(hWnd, hDC);							//"�����������" ��������
}