#include <windows.h>
#include <math.h>
LRESULT CALLBACK  WndProc(HWND, UINT, WPARAM, LPARAM);

int  main(void) {
	MSG msg;
	int  use = CW_USEDEFAULT;
	HWND  wnd = CreateWindowA("BUTTON", "�� ����", WS_OVERLAPPEDWINDOW, use, use, use, use,
		GetDesktopWindow(), NULL, (HINSTANCE)GetModuleHandle(NULL), NULL);
	if (!wnd)
		ExitProcess(1u);

	SetClassLong(wnd, GCL_HCURSOR, (LONG)LoadCursor(NULL, IDC_ARROW)); // ������
	SetClassLong(wnd, GCL_HBRBACKGROUND, (LONG)GetStockObject(WHITE_BRUSH)); // ���� �����
	SetWindowLong(wnd, GWL_WNDPROC, (LONG)WndProc);  // ���������� ���������

	ShowWindow(wnd, SW_SHOWDEFAULT);
	UpdateWindow(wnd);
	InvalidateRect(wnd, NULL, TRUE);

	ZeroMemory(&msg, sizeof(MSG));
	while (GetMessageA(&msg, NULL, 0U, 0U)) {
		DispatchMessageA(&msg);
		TranslateMessage(&msg);
	}
	DestroyWindow(wnd);
	return 0;
}

LRESULT CALLBACK  WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	PAINTSTRUCT ps;
	HGDIOBJ old;
	int x, y, ny;
	static POINT pt;
	static HPEN  pen;

	switch (msg) {
	case WM_SHOWWINDOW:
		pen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
		pt.x = pt.y = 0L;
		break;
	case WM_PAINT:   // ���������� ��������� �� �����
		BeginPaint(hwnd, &ps);

		// ������ ������ X,Y
		MoveToEx(ps.hdc, ps.rcPaint.right / 2, 10, NULL);
		LineTo(ps.hdc, ps.rcPaint.right / 2, ps.rcPaint.bottom - 10);

		MoveToEx(ps.hdc, 10, ps.rcPaint.bottom / 2, NULL);
		LineTo(ps.hdc, ps.rcPaint.right - 10, ps.rcPaint.bottom / 2);

		old = SelectObject(ps.hdc, pen);
		// ������ ��������������-�����
		y = ps.rcPaint.bottom / 2;
		for (pt.y = y + 10, pt.x = 10, x = 11; x < ps.rcPaint.right - 10; x++) {
			ny = y + ps.rcPaint.bottom / 8 * sin(2 * 3.14 * x / (45 * ps.rcPaint.right / 180));
			MoveToEx(ps.hdc, pt.x, pt.y, NULL);
			LineTo(ps.hdc, x, ny);
			pt.x = x;
			pt.y = ny;
		}
		SelectObject(ps.hdc, old);
		EndPaint(hwnd, &ps);
		break;
	case WM_DESTROY:
		DeleteObject(pen);
		PostQuitMessage(0);
		break;
	default: return DefWindowProcA(hwnd, msg, wParam, lParam);
	}
	return 0;
}