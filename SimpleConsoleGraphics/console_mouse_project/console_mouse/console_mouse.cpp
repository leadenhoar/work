#include <windows.h>
#include <stdio.h>

#define IMPORT __declspec (dllimport)
#pragma comment(lib,"MClick.lib") //���������� MClick.lib
//������� ������������� �� DLL
IMPORT DWORD getMouseClick(HANDLE hSTD_IN, COORD &POS); 
//������������ �������� POS - ���������� ������ ����
//(� ���������� ����������� ������� - ��� �����!)
//DWORD reurn :
//FROM_LEFT_1ST_BUTTON_PRESSED - 1-����� ������ (�����)
//FROM_LEFT_2ND_BUTTON_PRESSED - 2-����� ������ (�������, ���� ����)
//FROM_LEFT_3RD_BUTTON_PRESSED - 3-����� ������ (��� ������ 1, ���� ����)
//FROM_LEFT_4TH_BUTTON_PRESSED - 4-����� ������ (��� ������ 2, ���� ����)
//RIGHTMOST_BUTTON_PRESSED     - ������ ������  (������)   

#define RED RGB(255,0,0)
#define GRN RGB(0,255,0)
#define BLU RGB(0,0,255)

#define BLK RGB(0,0,0)
#define WHT RGB(255,255,255)

HPEN getPen(int iPEN_STYLE, int iPEN_SIZE, int iCOLORREF);
BOOL SetPoint(HDC hDC, HPEN hPen, COORD PNT);
BOOL PlotLine(HDC hDC, HPEN hPen, COORD BGN, COORD END);

COORD setCordScale(COORD POS, RECT pRECT);

int main()
{
	SetConsoleOutputCP(1251);
	HWND   hWnd     = GetForegroundWindow();
	HANDLE hSTD_IN  = GetStdHandle(STD_INPUT_HANDLE);
	HANDLE hSTD_OUT = GetStdHandle(STD_OUTPUT_HANDLE);

	HPEN	LPEN = getPen(PS_SOLID, 2, RED);
	HPEN	PPEN = getPen(PS_SOLID, 3, WHT);
	RECT	pRECT= {0};
	COORD	POS  = {0};
	DWORD	dwCLICK = NULL;
	GetWindowRect(hWnd,&pRECT);
	COORD	BGN  = setCordScale(POS,pRECT);
	HDC hDC = GetWindowDC(hWnd);
	if(hDC)
	{
		SetBkMode(hDC,TRANSPARENT);//����� � �� ������������� ���� �����, �� � ��� ����
		SetPoint (hDC, PPEN, BGN);
		do
		{
			GetWindowRect(hWnd,&pRECT);
			if((dwCLICK = getMouseClick(hSTD_IN,POS)) == RIGHTMOST_BUTTON_PRESSED)
			{
				//������� ������
				ShowWindow(hWnd,SW_HIDE);
				PlotLine(hDC, LPEN, BGN,  BGN);
				ShowWindow(hWnd,SW_SHOW);
			}
			POS = setCordScale(POS,pRECT);
			PlotLine(hDC, LPEN, BGN,  POS);
			SetPoint(hDC, PPEN, BGN = POS);
		}
		while(dwCLICK != FROM_LEFT_2ND_BUTTON_PRESSED);
	}
	system("pause");
	return 0;
}

HPEN getPen(int iPEN_STYLE, int iPEN_SIZE, int iCOLORREF)
{
	return CreatePen(iPEN_STYLE, iPEN_SIZE, iCOLORREF);
}

BOOL SetPoint(HDC hDC, HPEN hPen, COORD PNT)
{
	EXTLOGPEN pINFO;
	GetObject(hPen,sizeof(EXTLOGPEN),&pINFO);
	SelectObject(hDC,hPen);
	return Ellipse
	(
		hDC,
		PNT.X - pINFO.elpWidth,
		PNT.Y + pINFO.elpWidth, 
		PNT.X + pINFO.elpWidth,
		PNT.Y - pINFO.elpWidth
	);
}

BOOL PlotLine(HDC hDC, HPEN hPen, COORD BGN, COORD END)
{
	SelectObject(hDC,hPen);
	MoveToEx(hDC,BGN.X,BGN.Y,NULL);
	return LineTo(  hDC,END.X,END.Y);
}

COORD setCordScale(COORD POS, RECT pRECT)
{
	if(POS.X == 0)
		POS.X = 1;
	if(POS.Y == 0)
		POS.Y = 1;

	int nROWS = 25;
	int nCOLS = 80;

	double CX = (pRECT.right - pRECT.left)/nCOLS;
	double CY = (pRECT.bottom - pRECT.top)/nROWS;

	//��� ��������� ������(text mode) �����  �������  ��  ������
    //(80  ��� 40 ������� � ������ � 25 ����� � ������)
	POS.X *= CX;
	POS.Y *= CY;

	int xBORDER = GetSystemMetrics(SM_CXBORDER);//������ ������� ����
	int yBORDER = GetSystemMetrics(SM_CYMENU);  //������ ��������� ���� ~= ������ ����� ����
	int xDRAG	= GetSystemMetrics(SM_CXDRAG);  //����� �������� �� ����� �������� ����
	int yDRAG	= GetSystemMetrics(SM_CYDRAG);  //����� �������� �� ������ �������� ����

	POS.X += xBORDER + xDRAG;//������ ������� ���� + ����� �������� �� �������� ����
	POS.Y += yBORDER + yDRAG;
	return POS;
}

