#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class XOR {
private:
	ifstream fin;
	ofstream fout;
	string key, outtext;
public:
	XOR();
	XOR(const string&, const string&);
	XOR(const string&, const string&, const string&);
	~XOR();
	bool selectInputFile(const string&);
	bool selectOutputFile(const string&);
	void selectKey(const string&);
	void readFromFileAndEncrypt();
	void writeToFile();
};