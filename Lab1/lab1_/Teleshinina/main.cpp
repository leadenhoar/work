#include "Header.h"

int main(){
	Group students;
	int menu;

	do{
		system("cls");
		cout << "Menu:\n";
		cout << "1: add student\n";
		cout << "2: print students\n";
		cout << "3: read list from file\n";
		cout << "4: write list to file\n";
		cout << "5: clear list\n";
		cout << "6: exit\n";
		cout << "Run: ";
		cin >> menu;
		switch (menu){
		case 1:
			students.Add();
			break;
		case 2:
			students.Print();
			break;
		case 3:
			students.ReadList();
			break;
		case 4:
			students.WriteList();
			break;
		case 5:
			students.ClearList();
			break;
		}
	} while (menu != 6);

	system("pause");

	return 0;
}
