#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class StudentTeleshinina{
private:
	char s_name[255];
	char name[255];
	char surname[255];
	int age;
	double point;
public:
	void Init(char*, char*, char*, int, double);
	void Display();
	void ReadFile(ifstream&);
	void WriteFile(ofstream&);
	void Read();
};

class Group {
private:
	vector<StudentTeleshinina*> students;
	ifstream fin;
	ofstream fout;
public:
	Group();
	~Group();
	void Add();
	void Print();
	void ReadList();
	void WriteList();
	void ClearList();
};