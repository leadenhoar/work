#include "Header.h"

void StudentTeleshinina::Init(char *sn, char *n, char *sur, int a, double s){
	strcpy_s(s_name, sn);
	strcpy_s(name, n);
	strcpy_s(surname, sur);
	age = a;
	point = s;
}
void StudentTeleshinina::Display(){
	cout << "Student " << s_name << ' ' << name << ' ' << surname << ":\n";
	cout << "Age: " << age << endl;
	cout << "Point: " << point << "\n\n";
}
void StudentTeleshinina::ReadFile(ifstream &file){
	file >> s_name;
	file >> name;
	file >> surname;
	file >> age;
	file >> point;
}
void StudentTeleshinina::WriteFile(ofstream &file){
	file << s_name << ' ' << name << ' ' << surname << ' ' << age << ' ' << point << endl;
}

void StudentTeleshinina::Read(){
	char s_name[255];
	char name[255];
	char surname[255];
	double ser_bal;
	int age;

	system("cls");
	cout << "Enter second name: ";
	cin >> s_name;
	cout << "Enter name: ";
	cin >> name;
	cout << "Enter surnname: ";
	cin >> surname;
	cout << "Enter age: ";
	cin >> age;
	cout << "Enter point: ";
	cin >> ser_bal;
	this->Init(s_name, name, surname, age, ser_bal);
}

Group::Group(){
	fin.open("info.txt"); fout.open("info.txt");
}

Group::~Group(){
	ClearList(); fin.close(); fout.close();
}
 

void Group::Add(){
	StudentTeleshinina* pS = new StudentTeleshinina;
	pS->Read();
	students.push_back(pS);
	cout << "Success!\n";
	system("pause");
}

void Group::Print(){
	int i;

	system("cls");
	for (i = 0; i < students.size(); i++)
		students[i]->Display();
	if (students.size() == 0){
		cout << "list is empty...\n";
	}
	system("pause");
}

void Group::ReadList(){
	system("cls");
	ClearList();
	fin.open("info.txt");
	while (!fin.eof()){
		StudentTeleshinina* pS = new StudentTeleshinina;
		pS->ReadFile(fin);
		students.push_back(pS);
	}
	students.erase(students.end() - 1);
	cout << "Success!\n";
	system("pause");
	fin.close();
}

void Group::WriteList(){
	int i;

	system("cls");

	fout.open("info.txt");
	for (i = 0; i < students.size(); i++)
		students[i]->WriteFile(fout);
	cout << "Success!\n";
	system("pause");
	fout.close();
}

void Group::ClearList(){
	system("cls");
	for (size_t i = 0; i < students.size(); i++)
		delete students[i];
	students.clear();
	cout << "Success!\n";
	system("pause");
}