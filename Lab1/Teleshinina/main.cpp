#include "Header.h"
#include <iostream>

using namespace std;
int main(){
	Group students;
	int menu;

	do{
		system("cls");
		cout << "Menu:\n";
		cout << "1: add student\n";
		cout << "2: add captain\n";
		cout << "3: print students\n";
		cout << "4: read list from file\n";
		cout << "5: write list to file\n";
		cout << "6: clear list\n";
		cout << "7: exit\n";
		cout << "Run: ";
		cin >> menu;
		switch (menu){
		case 1:
			students.AddStudentTeleshinina();
			break;
		case 2:
			students.AddCaptainTeleshinina();
			break;
		case 3:
			students.Print();
			break;
		case 4:
		{
			cout << "Enter the path to the file" << endl;
			string filename;
			cin >> filename;
			if (students.ReadFile(filename))
			{
				cout << "Success!";
				cout << "For return to menu press any key ";  system("pause");
				system("cls");
			}
			else{
				cout << "Please, check your file!" << endl;
				cout << "For return to menu press any key ";  system("pause");
				system("cls");
			}
			break;
		}
			
		case 5:
		{
			cout << "Enter the path to the file:" << endl;
			string filename;
			cin >> filename;
			if (students.WriteFile(filename) == 1){
				cout << "Success!" << endl;
				cout << "For return to menu press any key ";  system("pause");
				system("cls");
			}
			else{
				cout << "Base is empty!" << endl;
				cout << "For return to menu press any key ";  system("pause");
				system("cls");
			}

			break;
		}
		case 6:
		{
			students.Clear();
			cout << "Success!" << endl;
			cout << "For return to menu  press any key ";  system("pause");
			system("cls");

		break;
		}
		case 7:
			return 0;
		default:
			cout << "ERROR!" << endl;
			break;

		}
	} while (true);

	system("pause");
	return 0;
}
