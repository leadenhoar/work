#include "Header.h"

//IMPLEMENT_SERIAL(CaptainTeleshinina, StudentTeleshinina, 1)

IMPLEMENT_DYNAMIC(StudentTeleshinina, CObject)

using namespace std;

StudentTeleshinina::StudentTeleshinina()
{

}

StudentTeleshinina::~StudentTeleshinina()
{

}

void StudentTeleshinina::ReadData()
{
	string s_name;
	string name;
	string surname;
	
	system("cls");
	cout << "Enter second name: " << endl;
	cin >> s_name;
	s_name_ = s_name.c_str();

	cout << "Enter name: " << endl;
	cin >> name;
	name_ = name.c_str();

	cout << "Enter surnname: " << endl;
	cin >> surname;
	surname_ = surname.c_str();
		
	cout << "Enter age: " << endl;
	cin >> age_;

	cout << "Enter point: " << endl;
	cin >> point_;
}

void StudentTeleshinina::WriteData()
{
	cout << "Second name: "  << s_name_ << endl;
	cout << "Name: "	     << name_ << endl;
	cout << "Surname: "      << surname_ << endl;
	cout << "Age: "          << age_ << endl;
	cout << "Point: "        << point_ << endl;
}

void StudentTeleshinina::Serialize(CArchive &archive)
{
	//CObject::Serialize(archive);
	if (archive.IsStoring())
		archive << s_name_ << name_ << surname_ << age_ << point_;
	else
		archive >> s_name_ >> name_ >> surname_ >> age_ >> point_;
}


IMPLEMENT_DYNAMIC(CaptainTeleshinina, CObject)

CaptainTeleshinina::CaptainTeleshinina()
{

}

CaptainTeleshinina::~CaptainTeleshinina()
{

}

void CaptainTeleshinina::ReadData()
{
	StudentTeleshinina::ReadData();
	cout << "Enter id: " << endl;
	cin >> id_;
}

void CaptainTeleshinina::WriteData()
{
	StudentTeleshinina::WriteData();
	cout << "Id: " << id_ << endl;
	
}

void CaptainTeleshinina::Serialize(CArchive &archive)
{
	StudentTeleshinina::Serialize(archive);
	
	if (archive.IsStoring())
		archive << id_;
	else
		archive >> id_;
}

Group::Group()
{
	
}

Group::~Group()
{
	
}

void Group::AddStudentTeleshinina(){
	system("cls");
	shared_ptr<StudentTeleshinina> student = make_shared<StudentTeleshinina>();
	student->ReadData();
	students.push_back(student);
	cout << "Successfully student added!\n";
	system("pause");
}

void Group::AddCaptainTeleshinina(){
	system("cls");
	shared_ptr<CaptainTeleshinina> captain = make_shared<CaptainTeleshinina>();
	captain->ReadData();
	students.push_back(captain);
	cout << "Successfully captain added!\n";
	system("pause");
}

void Group::Print()
{
system("cls");
	for (auto sti = students.begin(); sti != students.end(); ++sti)
		(*sti)->WriteData();
	if (students.size() == 0){
		cout << "grioup is empty...\n";
	}
	system("pause");
}

bool Group::ReadFile(string filename){
	system("cls");
	Clear();
	CFile fin;
	if (!fin.Open(CString(filename.c_str()), CFile::modeRead))
		return 0;
	
	CArchive archive(&fin, CArchive::load);
	size_t size;
	archive >> size;
	
	StudentTeleshinina *student = new StudentTeleshinina();
	for (size_t i = 0; i < size; ++i)
	{
		student->Serialize(archive);
		students.push_back(shared_ptr<StudentTeleshinina>(student));
	}
	
	cout << "Successfully group reserialized!\n";
	system("pause");
	archive.Close();
	fin.Close();
	return 1;
}

bool Group::WriteFile(string filename)
{
	CFile fout;
	if (!fout.Open(CString(filename.c_str()), CFile::modeWrite | CFile::modeCreate))
		return 0;

	CArchive archive(&fout, CArchive::store);	
	archive << students.size();
	
	for (size_t sti = 0; sti < students.size(); ++sti)
	{
		(*students[sti]).Serialize(archive);
	}

	cout << "Successfully group serialized!\n";
	system("pause");
	archive.Close();
	fout.Close();
	return 1;
}

void Group::Clear(){
	system("cls");
	students.clear();
	cout << "Successfully cleared!\n";
	system("pause");
}