#pragma once
#include <SDKDDKVer.h>
#include <string>
#include <iostream>
#include <fstream>

#include <cctype>
#include <afx.h>
#include <cctype>
#include <vector>
#include <iterator>
#include <memory>




using namespace std;
class StudentTeleshinina
	: public CObject
{
public:
	//DECLARE_SERIAL(StudentTeleshinina)
	DECLARE_DYNAMIC(StudentTeleshinina)
	StudentTeleshinina();
	~StudentTeleshinina();
	virtual void ReadData();
	virtual void WriteData();
	virtual void Serialize(CArchive &archive);
private:
	CString s_name_;
	CString name_;
	CString surname_;
	size_t  age_;
	double  point_;
};

class CaptainTeleshinina
	: public StudentTeleshinina
{
private:
	size_t id_;

public:
	DECLARE_DYNAMIC(CaptainTeleshinina)
	//DECLARE_SERIAL(CaptainTeleshinina)
	CaptainTeleshinina();
	~CaptainTeleshinina();
	void ReadData();
	void WriteData();
	void Serialize(CArchive &archive);
};

class Group {
private:
	std::vector<std::shared_ptr<StudentTeleshinina>> students;
	
public:
	Group();
	~Group();
	void AddStudentTeleshinina();
	void AddCaptainTeleshinina();
	void Print();
	bool ReadFile(std::string filename);
	bool WriteFile(std::string filename);
	void Clear();
};

