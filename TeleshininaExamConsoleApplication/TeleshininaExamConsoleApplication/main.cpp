#include <iostream>
#include <fstream>
#include <string>
#include <algorithm> //����� ������� swap
using namespace std;

int main()
{
	cout << "Input file:" << endl;
	string in_path, out_path;
	//��������� ��� �������� �����, ���� �� ������ ����, �� �� ����� ����� ���� �� ����� � �����
	cin >> in_path;
	//������� ������� ����
	ifstream input(in_path);
	//������� �������� ����
	ofstream output(in_path + "_inv.txt");

	string str;
	//����������� ����
	while (true)
	{
		//������� ������(���� �� ���������� ������� ������)
		getline(input, str);
		//���� ����� ���������� �������� ����� ����������, �� ������ ��� �������, ������� �� �����
		if (!input) break;

		//��������� ���� ����� ������������� ������
		for (size_t i = 0; i < str.size()/2; ++i)
		{
			//������ ������� �������� � �������� [i] � [����� ������ - 1 - i]
			swap(str[i], str[str.size() - 1 - i]);
		}
		//������� � �������� ����
		output << str << endl;
	}
}