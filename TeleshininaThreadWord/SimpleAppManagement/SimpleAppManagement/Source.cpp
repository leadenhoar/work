#include "windows.h"
#include <limits> 
#include "process.h"
#include <vector>
#include <iostream>
#include <array>

#if defined(max)
#undef max
#endif

using namespace std;
int main()
{
	std::vector < PROCESS_INFORMATION > processes_;

	while (true)
	{
		char command_;

		std::cout << "Press n for new Notepad instance or x for Exit\n";
		std::cin >> command_;

		switch (command_)
		{
			case 'n':
			case 'N':
			{
						std::array<TCHAR, 1024> args_ = {};

						PROCESS_INFORMATION info_;

						STARTUPINFO details_ = {};

						details_.cb = sizeof(details_);

						if (CreateProcess(("C:\\Windows\\System32\\Notepad.exe"), args_.data(), NULL, NULL, false, 0, NULL, NULL, &details_, &info_))
							processes_.push_back(info_);

						break;
			}

			case 'x':
			case 'X':
			{
						for (auto process_ : processes_)
						{
							TerminateProcess(process_.hProcess, 0);

							CloseHandle(process_.hProcess);
							CloseHandle(process_.hThread);
						}

						processes_.resize(0);
			}

			default:
			{
					   std::cin.clear();
					   std::cout << "Invalid input";
					   cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
		}
	}
}