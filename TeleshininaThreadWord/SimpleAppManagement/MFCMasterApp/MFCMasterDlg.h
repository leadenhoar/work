
// MFCMasterDlg.h : header file
//

#pragma once
#include <vector>
#include "process.h"


// CMasterDlg dialog
class CMasterDlg : public CDialogEx
{
// Construction
public:
	CMasterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MFCMASTERAPP_DIALOG };
	std::vector < PROCESS_INFORMATION > processes;
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnStart();
	afx_msg void OnBnClickedBtnStop();
	afx_msg void OnBnClickedBtnClose();
};
