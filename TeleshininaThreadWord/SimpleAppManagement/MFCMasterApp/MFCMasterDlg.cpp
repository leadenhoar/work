
// MFCMasterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MFCMasterApp.h"
#include "MFCMasterDlg.h"
#include "afxdialogex.h"
#include <array>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMasterDlg dialog



CMasterDlg::CMasterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMasterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMasterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMasterDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_START, &CMasterDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &CMasterDlg::OnBnClickedBtnStop)
	ON_BN_CLICKED(IDC_BTN_CLOSE, &CMasterDlg::OnBnClickedBtnClose)
END_MESSAGE_MAP()


// CMasterDlg message handlers

BOOL CMasterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMasterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMasterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMasterDlg::OnBnClickedBtnStart()
{
	// TODO: Add your control notification handler code here
	LPCTSTR appName = "C:\\Windows\\System32\\Notepad.exe";
	std::array<TCHAR, 1024> cmdArgs = {};
	PROCESS_INFORMATION info;
	STARTUPINFO details = {};
	ZeroMemory(&details, sizeof(details));
	details.cb = sizeof(details);
	if (CreateProcess(appName, cmdArgs.data(), NULL, NULL, false, 0, NULL, NULL, &details, &info))
	{
		processes.push_back(info);
	}
}


void CMasterDlg::OnBnClickedBtnStop()
{
	
	/*if (GetExitCodeProcess(processes.back()) == STILL_ACTIVE)
	{
		MessageBox("Alive");
	}*/

	for (auto process : processes)
	{
		DWORD exitCode;
		if (GetExitCodeProcess(process.hProcess, &exitCode))
		{
			//Check the code here
			if (exitCode == STILL_ACTIVE)
			{
				MessageBox("Still alive");
			}
			//else
				//printf("code = %X\n", exitCode);
		}
		else
		{
			MessageBox("Failed to get exit code");
		}

		TerminateProcess(process.hProcess, 0);

		CloseHandle(process.hProcess);
		CloseHandle(process.hThread);
	}

	processes.resize(0);
	// TODO: Add your control notification handler code here
}


void CMasterDlg::OnBnClickedBtnClose()
{
	OnBnClickedBtnStop();
	OnCancel();
	// TODO: Add your control notification handler code here
}
