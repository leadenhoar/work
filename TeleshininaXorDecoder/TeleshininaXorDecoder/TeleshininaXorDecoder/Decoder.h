#ifndef DECODER_H
#define DECODER_H
#include <string>
#include <vector>
#include <cstdint>
#include <fstream>

using namespace std;
class DecoderTeleshinina
{
private:
	string input_source_path;
	string input_original_path;
	string output_decoded_path;
	string table_path;
	
	ifstream inSource;
	ifstream inOriginal;
	ofstream outDecoded;

	vector<uint8_t> sourceTextV;
	vector<uint8_t> originalTextV;
	vector<uint8_t> decodedTextV;
	vector<uint8_t> tmpTextV;

	vector<uint32_t> freqTableSourceV;
	vector<double>   relFreqTableSourceV;
	vector<uint32_t> freqTableOriginalV;
	vector<double>   relFreqTableOriginalV;
	vector<uint32_t> freqTableTmpV;
	vector<double>   relFreqTableTmpV;
	vector<bool>     searched;
	vector<uint8_t> codeToOriginal;

public:
	
	DecoderTeleshinina();
	DecoderTeleshinina(string const input_source_path
		, string const input_original_path
		, string const output_decoded_path);

	void selectInputSourcePath(string const path);
	void selectInputOriginalPath(string const path);
	void selectOutputDecodedPath(string const path);
	void selectInputTablePath(string const path);

	void decode();
	void decodeWithTable();

	bool openSourceFIle();
	bool openOriginalFIle();
	bool openOutputFIle();

	bool isOpenSourceFIle();
	bool isOpenOriginalFIle();
	bool isOpenOutputFIle();

	bool readSourceFile();
	bool readOriginalFile();
	bool writeOutputFile();
	bool writeTableToFile();
	bool readTableFromFile();

	bool closeSourceFIle();
	bool closeOriginalFIle();
	bool closeOutputFIle();

	

	string  getSourceText();
	string  getOriginalText();
	string  getDecodedText();

	vector<uint32_t> const& getFreqTableSourceV();
	vector<double>   const& getRelFreqTableSourceV();
	vector<uint32_t> const& getFreqTableOriginalV();
	vector<double>   const& getRelFreqTableOriginalV();
	
	vector<uint8_t> const& getCodeToOriginal();
	vector<uint8_t> & getCodeToOriginalToEdit();
	void buildFreqTable(vector<uint8_t> const &text, vector<uint32_t> &table, vector<double> &relTable);
	double countIndex(vector<uint8_t> const &text);
	double countEngIndex(vector<uint8_t> const &text);
	double countRusIndex(vector<uint8_t> const &text);
	void keyLenSearch();

	pair<vector<uint32_t>  const &, vector<double> const &> getOrigTable();
	

	
	~DecoderTeleshinina();
};

#endif //DECODER_H