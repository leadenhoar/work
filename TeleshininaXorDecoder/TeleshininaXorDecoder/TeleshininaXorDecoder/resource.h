//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TeleshininaXorDecoder.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TELESHININAXORDECODER_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDD_DLG_EDIT                    129
#define IDC_EDIT_FILEIN                 1001
#define IDC_EDIT2                       1002
#define IDC_INPUT_TEXT                  1002
#define IDC_DEC_CH                      1002
#define IDC_BTN_GETFILEIN               1003
#define IDC_EDIT_TABLE                  1004
#define IDC_EDIT_FILEOUT                1005
#define IDC_BTN_GETFILEOUT              1006
#define IDC_BUTTON3                     1007
#define IDC_BTN_PROCESS                 1007
#define IDC_INPUT_ORIGTEXT              1008
#define IDC_EDIT_ORIGFILEIN             1010
#define IDC_OUTPUT_DECODED              1011
#define IDC_BTN_GET_ORIGFILEIN          1012
#define IDC_DECODED_FREQ_LIST           1017
#define IDC_BTN_SAVA_DECODED            1018
#define IDC_BTN_EDIT_TABLE              1019
#define IDC_BTN_SAVE_TABLE              1020
#define IDC_BTN_LOAD_TABLE              1021
#define IDC_BTN_REPROCESS               1022
#define IDC_ENC_CH                      1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
