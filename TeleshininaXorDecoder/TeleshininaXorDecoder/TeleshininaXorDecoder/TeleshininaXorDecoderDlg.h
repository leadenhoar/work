
// TeleshininaXorDecoderDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <vector>
#include <cstdint>


// ���������� ���� CTeleshininaXorDecoderDlg
class CTeleshininaXorDecoderDlg : public CDialogEx
{
	// ��������
public:
	CTeleshininaXorDecoderDlg(CWnd* pParent = NULL);	// ����������� �����������

	// ������ ����������� ����
	enum { IDD = IDD_TELESHININAXORDECODER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


	// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnGetFilein();
	CString m_sFilein;

	afx_msg void OnBnClickedBtnGetfileout();

	CString m_sFileout;
	afx_msg void OnProcess();
	BOOL ProcessFiles();
	//	CString m_sDecodedText;
	CString m_sOrigFilein;
	afx_msg void OnGetOrigfilein();
	afx_msg void OnEnChangeInputText2();
	CString m_sOriginalText;
	afx_msg void OnEnChangeInputOrigtext2();
	CString m_sSourceText;
	CString m_sDecodedText;
	CListCtrl m_sDecFreqLst;
	void showFreqTable(std::vector<std::uint32_t> const & freqTable, std::vector<double> const & relFreqTable);
	void showDecodedFreqTable(std::vector<std::uint8_t> decoded, std::vector<std::uint32_t> freqTable, std::vector<double> relFreqTable);
	afx_msg void OnSavaDecoded();
	afx_msg void OnSaveTable();
	afx_msg void OnBtnEditTable();
	afx_msg void OnBtnLoadTable();
	CString m_sTable;
	afx_msg void OnBtnReprocess();
};
