#include "stdafx.h"
#include "Decoder.h"
#include <string>
#include <vector>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <utility>


using namespace std;

DecoderTeleshinina::DecoderTeleshinina()
{
	size_t reserved_text_size = (size_t)1e4;
	sourceTextV.reserve(reserved_text_size);
	originalTextV.reserve(reserved_text_size);
	decodedTextV.reserve(reserved_text_size);

}

DecoderTeleshinina::DecoderTeleshinina(string const input_source_path
	, string const input_original_path
	, string const output_decoded_path)
	: input_source_path(input_source_path)
	, input_original_path(input_original_path)
	, output_decoded_path(output_decoded_path)
{
	size_t reserved_text_size = (size_t)1e4;
	sourceTextV.reserve(reserved_text_size);
	originalTextV.reserve(reserved_text_size);
	decodedTextV.reserve(reserved_text_size);
}

void DecoderTeleshinina::selectInputSourcePath(string const path)
{
	input_source_path = path;
}
void DecoderTeleshinina::selectInputOriginalPath(string path)
{
	input_original_path = path;
}
void DecoderTeleshinina::selectOutputDecodedPath(string path)
{
	output_decoded_path = path;
}
void DecoderTeleshinina::selectInputTablePath(string const path)
{
	table_path = path;
}

void DecoderTeleshinina::decode()
{
	codeToOriginal.clear();
	codeToOriginal.resize(256, 0);
	searched.clear();
	searched.resize(256, false);
	buildFreqTable(sourceTextV, freqTableSourceV, relFreqTableSourceV);
	buildFreqTable(originalTextV, freqTableOriginalV, relFreqTableOriginalV);

	
	for (size_t itSFT = 0; itSFT < freqTableSourceV.size(); ++itSFT)
	{
		if (freqTableSourceV[itSFT])
		{
			double min_delta = 1;
			size_t lastItSFT = 0;
			for (size_t itOFT = 0; itOFT < freqTableOriginalV.size(); ++itOFT)
			{
				double delta = abs(relFreqTableSourceV[itSFT] - relFreqTableOriginalV[itOFT]);
				if (delta < min_delta && !searched[itOFT])
				{
					lastItSFT = itOFT;
					min_delta = delta;
					codeToOriginal[itSFT] = itOFT;
					
				}
			}
			searched[lastItSFT] = true;
		}
	}
	decodedTextV.clear();
	decodedTextV.reserve(sourceTextV.size());
	for (size_t itSoText = 0; itSoText < sourceTextV.size(); ++itSoText)
	{
		decodedTextV.push_back(codeToOriginal[sourceTextV[itSoText]]);
	}
}
void DecoderTeleshinina::decodeWithTable()
{
	decodedTextV.clear();
	decodedTextV.reserve(sourceTextV.size());
	for (size_t itSoText = 0; itSoText < sourceTextV.size(); ++itSoText)
	{
		decodedTextV.push_back(codeToOriginal[sourceTextV[itSoText]]);
	}
}

bool DecoderTeleshinina::openSourceFIle()
{
	if (inSource.is_open())
	{
		inSource.close();
	}
	inSource.open(input_source_path.c_str(), ios_base::binary);
	return inSource.is_open();
}
bool DecoderTeleshinina::openOriginalFIle()
{
	if (inOriginal.is_open())
	{
		inOriginal.close();
	}
	inOriginal.open(input_original_path, ios_base::binary);
	return inOriginal.is_open();
}
bool DecoderTeleshinina::openOutputFIle()
{
	if (outDecoded.is_open())
	{
		outDecoded.close();
	}
	outDecoded.open(output_decoded_path, ios_base::binary);
	return outDecoded.is_open();
}

bool DecoderTeleshinina::isOpenSourceFIle()
{
	return inSource.is_open();
}
bool DecoderTeleshinina::isOpenOriginalFIle()
{
	return inOriginal.is_open();
}
bool DecoderTeleshinina::isOpenOutputFIle()
{
	return outDecoded.is_open();
}

bool DecoderTeleshinina::readSourceFile()
{
	sourceTextV.clear();
	uint8_t curChar = 0;
	while (inSource.read(reinterpret_cast<char *>(&curChar), sizeof(curChar)))
	{
		sourceTextV.push_back(curChar);
	}
	return inSource.eof();
}
bool DecoderTeleshinina::readOriginalFile()
{
	originalTextV.clear();
	uint8_t curChar = 0;
	while (inOriginal.read(reinterpret_cast<char *>(&curChar), sizeof(curChar)))
	{
		originalTextV.push_back(curChar);
	}
	return inOriginal.eof();
}
bool DecoderTeleshinina::writeOutputFile()
{

	uint8_t curChar = 0;
	size_t i = 0;

	for (; i < decodedTextV.size(); ++i)
	{
		outDecoded.write(reinterpret_cast<char *>(&decodedTextV[i]), sizeof(decodedTextV[i]));
	}

	return i == decodedTextV.size();
}
bool DecoderTeleshinina::writeTableToFile()
{

	ofstream table(table_path);
	

	for (size_t itCode = 0; itCode < freqTableSourceV.size(); ++itCode)
	{
		if (freqTableSourceV[itCode])
		{
			size_t itOriginal = codeToOriginal[itCode];
			//table.width(15);
			table << left << setw(15) << itCode     << setw(15) << freqTableSourceV[itCode]       << setw(15) << relFreqTableSourceV[itCode];
			//table.width(15);
			table << left << setw(15) << itOriginal << setw(15) << freqTableOriginalV[itOriginal] << setw(15) << relFreqTableOriginalV[itOriginal] << endl;
		}
	}
	table.close();
	return true;
}
bool DecoderTeleshinina::readTableFromFile()
{
	freqTableSourceV.clear();
	freqTableSourceV.resize(256, 0);
	relFreqTableSourceV.clear();
	relFreqTableSourceV.resize(256, 0);
	freqTableOriginalV.clear();
	freqTableOriginalV.resize(256, 0);
	relFreqTableOriginalV.clear();
	relFreqTableOriginalV.resize(256, 0);

	codeToOriginal.clear();
	codeToOriginal.resize(256, 0);

	ifstream table(table_path);
	size_t itOriginal = 0, itCode = 0;
	while (table)
	{
		table >> itCode;
		table >> freqTableSourceV[itCode] >> relFreqTableSourceV[itCode];
		table >> itOriginal;
		codeToOriginal[itCode] = itOriginal;
		table >> freqTableOriginalV[itOriginal] >> relFreqTableOriginalV[itOriginal];
		
		if (table.eof())
		{
			break;
		}
	}
	table.close();
	return true;
}

bool DecoderTeleshinina::closeSourceFIle()
{
	inSource.close();
	return !inSource.is_open();
}
bool DecoderTeleshinina::closeOriginalFIle()
{
	inOriginal.close();
	return !inOriginal.is_open();
}
bool DecoderTeleshinina::closeOutputFIle()
{
	outDecoded.close();
	return !outDecoded.is_open();
}

string  DecoderTeleshinina::getSourceText()
{
	string outText;
	outText.reserve(sourceTextV.size());
	for (size_t i = 0; i < sourceTextV.size(); ++i)
	{
		outText += sourceTextV[i];
	}
	return outText;
}
string  DecoderTeleshinina::getOriginalText()
{
	string outText;
	outText.reserve(originalTextV.size());
	for (size_t i = 0; i < originalTextV.size(); ++i)
	{
		outText += originalTextV[i];
	}
	return outText;
}
string  DecoderTeleshinina::getDecodedText()
{
	string outText;
	outText.reserve(decodedTextV.size());
	for (size_t i = 0; i < decodedTextV.size(); ++i)
	{
		outText += decodedTextV[i];
	}
	return outText;
}

vector<uint32_t> const& DecoderTeleshinina::getFreqTableSourceV()
{
	return freqTableSourceV;
}
vector<double>   const& DecoderTeleshinina::getRelFreqTableSourceV()
{
	return relFreqTableSourceV;
}
vector<uint32_t> const& DecoderTeleshinina::getFreqTableOriginalV()
{
	return freqTableOriginalV;
}
vector<double>   const& DecoderTeleshinina::getRelFreqTableOriginalV()
{
	return relFreqTableOriginalV;
}

vector<uint8_t>  const& DecoderTeleshinina::getCodeToOriginal()
{
	return codeToOriginal;
}
vector<uint8_t>  & DecoderTeleshinina::getCodeToOriginalToEdit()
{
	return codeToOriginal;
}


void DecoderTeleshinina::buildFreqTable(vector<uint8_t> const &text, vector<uint32_t> &table, vector<double> &relTable)
{
	table.clear();
	table.resize(256, 0);

	relTable.clear();
	relTable.resize(256, 0);
	for (size_t i = 0; i < text.size(); ++i)
	{
		table[text[i]] += 1;
	}

	for (size_t i = 0; i < table.size(); ++i)
	{
		relTable[i] = (double)table[i] / (double)text.size();
	}
}

double DecoderTeleshinina::countIndex(vector<uint8_t> const &text)
{
	freqTableTmpV.clear();
	freqTableTmpV.resize(256, 0);

	relFreqTableTmpV.clear();
	relFreqTableTmpV.resize(256, 0);

	buildFreqTable(text, freqTableTmpV, relFreqTableTmpV);
	double index = 0;

	for (size_t i = 0; i < relFreqTableTmpV.size(); ++i)
	{

		index += (relFreqTableTmpV[i] * relFreqTableTmpV[i]);

	}

	return index;
}
double DecoderTeleshinina::countEngIndex(vector<uint8_t> const &text)
{

	freqTableTmpV.clear();
	freqTableTmpV.resize(256, 0);

	relFreqTableTmpV.clear();
	relFreqTableTmpV.resize(256, 0);

	buildFreqTable(text, freqTableTmpV, relFreqTableTmpV);
	double index = 0;

	for (size_t i = 0; i < freqTableTmpV.size(); ++i)
	{
		if ((text[i] > 64 && text[i] < 91) || (text[i] > 96 && text[i] < 123))
		{
			index += (relFreqTableTmpV[i] * relFreqTableTmpV[i]);
		}
	}

	return index;
}
double DecoderTeleshinina::countRusIndex(vector<uint8_t> const &text)
{
	{

		freqTableTmpV.clear();
		freqTableTmpV.resize(256, 0);

		relFreqTableTmpV.clear();
		relFreqTableTmpV.resize(256, 0);

		buildFreqTable(text, freqTableTmpV, relFreqTableTmpV);
		double index = 0;

		for (size_t i = 0; i < freqTableTmpV.size(); ++i)
		{
			if (text[i] > 191)
			{
				index += (relFreqTableTmpV[i] * relFreqTableTmpV[i]);
			}
		}

		return index;
	}
}

void DecoderTeleshinina::keyLenSearch()
{
	/*double best_index = 0;
	size_t key_len = 1;

	size_t orTextSize = originalTextV.size();
	tmpTextV.resize(orTextSize, 0);

	double best_index = 0;
	size_t key_len = 1;
	buildFreqTable(originalTextV, freqTableOriginalV, relFreqTableOriginalV);

	for (size_t offset = 1; offset < 40; offset++)
	{
	double index = 0;
	for (size_t i = 0; i < orTextSize; ++i)
	{
	tmpTextV[i] = originalTextV[i] ^ originalTextV[(i + offset) % orTextSize];
	}
	index = countIndex(tmpTextV);
	if (index > best_index)
	{
	best_index = index;
	key_len = offset;
	}
	}*/
}
pair<vector<uint32_t> const &, vector<double> const &> DecoderTeleshinina::getOrigTable()
{
	return pair<vector<uint32_t> const &, vector<double> const &>(freqTableOriginalV, relFreqTableOriginalV);
}

DecoderTeleshinina::~DecoderTeleshinina()
{

}

