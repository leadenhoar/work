// EditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TeleshininaXorDecoder.h"
#include "EditDlg.h"
#include "afxdialogex.h"


// CEditDlg dialog

IMPLEMENT_DYNAMIC(CEditDlg, CDialog)

CEditDlg::CEditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditDlg::IDD, pParent)
	, m_sEncChar(_T(""))
	, m_sDecChar(_T(""))
{

}

CEditDlg::~CEditDlg()
{
}

void CEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ENC_CH, m_sEncChar);
	//  DDX_Control(pDX, IDC_DEC_CH, m_sDecChar);
	//  DDX_Control(pDX, IDC_DEC_CH, m_sDecChar);
	DDX_Text(pDX, IDC_DEC_CH, m_sDecChar);
}


BEGIN_MESSAGE_MAP(CEditDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CEditDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CEditDlg message handlers


void CEditDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}
