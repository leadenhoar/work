
// TeleshininaXorDecoderDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "TeleshininaXorDecoder.h"
#include "TeleshininaXorDecoderDlg.h"
#include "afxdialogex.h"
#include "Decoder.h"
#include <vector>
#include <cstdint>
#include "EditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace decoder_nmsp {
	static DecoderTeleshinina decoder;
}

// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

// ����������
protected:
	DECLARE_MESSAGE_MAP()
public:
	BOOL ProcessFiles(CString sFileIn, CString sOrigFileIn, CString sFileOut);
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CTeleshininaXorDecoderDlg



CTeleshininaXorDecoderDlg::CTeleshininaXorDecoderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTeleshininaXorDecoderDlg::IDD, pParent)
	, m_sFilein(_T(""))	
	, m_sFileout(_T(""))
	, m_sOrigFilein(_T(""))
	, m_sOriginalText(_T(""))
	, m_sSourceText(_T(""))
	, m_sDecodedText(_T(""))
	, m_sTable(_T(""))
	
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTeleshininaXorDecoderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_FILEIN, m_sFilein);


	DDX_Text(pDX, IDC_EDIT_FILEOUT, m_sFileout);
	DDX_Text(pDX, IDC_EDIT_ORIGFILEIN, m_sOrigFilein);
	DDX_Text(pDX, IDC_INPUT_ORIGTEXT, m_sOriginalText);
	DDX_Text(pDX, IDC_INPUT_TEXT, m_sSourceText);
	DDX_Text(pDX, IDC_OUTPUT_DECODED, m_sDecodedText);
	DDX_Control(pDX, IDC_DECODED_FREQ_LIST, m_sDecFreqLst);


	DDX_Text(pDX, IDC_EDIT_TABLE, m_sTable);
}

BEGIN_MESSAGE_MAP(CTeleshininaXorDecoderDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_GETFILEIN, &CTeleshininaXorDecoderDlg::OnGetFilein)
	ON_BN_CLICKED(IDC_BTN_GETFILEOUT, &CTeleshininaXorDecoderDlg::OnBnClickedBtnGetfileout)
	ON_BN_CLICKED(IDC_BTN_PROCESS, &CTeleshininaXorDecoderDlg::OnProcess)
	ON_BN_CLICKED(IDC_BTN_GET_ORIGFILEIN, &CTeleshininaXorDecoderDlg::OnGetOrigfilein)
	
	
	ON_BN_CLICKED(IDC_BTN_SAVA_DECODED, &CTeleshininaXorDecoderDlg::OnSavaDecoded)
	ON_BN_CLICKED(IDC_BTN_SAVE_TABLE, &CTeleshininaXorDecoderDlg::OnSaveTable)
	ON_BN_CLICKED(IDC_BTN_EDIT_TABLE, &CTeleshininaXorDecoderDlg::OnBtnEditTable)
	ON_BN_CLICKED(IDC_BTN_LOAD_TABLE, &CTeleshininaXorDecoderDlg::OnBtnLoadTable)
	ON_BN_CLICKED(IDC_BTN_REPROCESS, &CTeleshininaXorDecoderDlg::OnBtnReprocess)
END_MESSAGE_MAP()


// ����������� ��������� CTeleshininaXorDecoderDlg

BOOL CTeleshininaXorDecoderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������
	
	m_sFilein = "";
	m_sOrigFilein = "";
	m_sFileout = "";
	m_sTable = "";
	m_sDecFreqLst.InsertColumn(0, "Coded", LVCFMT_LEFT, 55);
	m_sDecFreqLst.InsertColumn(1, "Frequency", LVCFMT_LEFT, 60);
	m_sDecFreqLst.InsertColumn(2, "Relative frequency", LVCFMT_LEFT, 105);
	m_sDecFreqLst.InsertColumn(3, "Decoded", LVCFMT_LEFT, 55);
	m_sDecFreqLst.InsertColumn(4, "Frequency", LVCFMT_LEFT, 60);
	m_sDecFreqLst.InsertColumn(5, "Relative frequency", LVCFMT_LEFT, 105);
	UpdateData(FALSE);
	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CTeleshininaXorDecoderDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CTeleshininaXorDecoderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CTeleshininaXorDecoderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

CString getWorkingDirectory()
{
	size_t length = 0;
	char *buffer;
	length = GetCurrentDirectory(0, NULL);
	buffer = new char[length];
	GetCurrentDirectory((DWORD)length, (LPTSTR)buffer);
	CString  dir(buffer);
	delete[] buffer;
	return dir;
}

void CTeleshininaXorDecoderDlg::OnGetFilein()
{
	using namespace decoder_nmsp;
	
	CFileDialog dlg(TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_sFilein = dlg.m_ofn.lpstrFile;
		UpdateData(FALSE);

		decoder.selectInputSourcePath((LPCTSTR)m_sFilein);

		
		if (!decoder.openSourceFIle())
		{

			CString sMsg;
			sMsg.Format("Can't open source input file: %s", m_sFilein);
			MessageBox(sMsg, "Error", MB_OK | MB_ICONERROR);

		}

		try
		{
			decoder.readSourceFile();
			CString text = decoder.getSourceText().substr(0, 500).c_str();
			//MessageBox(text);
			m_sSourceText = text;
			UpdateData(FALSE);

		}
		catch (CException* e)
		{
			MessageBox(_T("Error reading file"));
			e->Delete();
		}
	}
	
}

void CTeleshininaXorDecoderDlg::OnGetOrigfilein()
{
	using namespace decoder_nmsp;
	
	CFileDialog dlg(TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_sOrigFilein = dlg.m_ofn.lpstrFile;
		UpdateData(FALSE);


		decoder.selectInputOriginalPath((LPCTSTR)m_sOrigFilein);
		if (!decoder.openOriginalFIle())
		{

			CString sMsg;
			sMsg.Format("Can't open original input file: %s", m_sOrigFilein);
			MessageBox(sMsg, "Error", MB_OK | MB_ICONERROR);

		}

		try
		{
			decoder.readOriginalFile();
			CString text = decoder.getOriginalText().substr(0, 500).c_str();
			//MessageBox(text);
			m_sOriginalText = text;
			UpdateData(FALSE);

		}
		catch (CException* e)
		{
			MessageBox(_T("Error reading file"));
			e->Delete();
		}
	}
}

void CTeleshininaXorDecoderDlg::OnBnClickedBtnGetfileout()
{
	using namespace decoder_nmsp;

	CFileDialog dlg(FALSE);
	if (dlg.DoModal() == IDOK)
	{
		m_sFileout = dlg.m_ofn.lpstrFile;
		UpdateData(FALSE);

		decoder.selectOutputDecodedPath((LPCTSTR)m_sFileout);
		if (!decoder.openOutputFIle())
		{
			CString sMsg;
			sMsg.Format("Can't open output file: %s", m_sFileout);
			MessageBox(sMsg, "Error", MB_OK | MB_ICONERROR);

		}
	}
	
}

void CTeleshininaXorDecoderDlg::OnProcess()
{
	if (m_sFilein == "" || m_sOrigFilein == "")
	{
		MessageBox("Please chouse source text file and original text file", "Error", MB_OK | MB_ICONERROR);
		return;
	}
	
	using namespace decoder_nmsp;
	try
	{
		decoder.decode();
		m_sTable = "not saved";//For "Reprocess" button correct work
		showFreqTable(decoder.getFreqTableSourceV(), decoder.getRelFreqTableSourceV());
		showDecodedFreqTable(decoder.getCodeToOriginal(), decoder.getFreqTableOriginalV(), decoder.getRelFreqTableOriginalV());

		CString text = decoder.getDecodedText().substr(0, 500).c_str();
		//MessageBox(text);
		m_sDecodedText = text;
		UpdateData(FALSE);
		MessageBox("Successful processing done", "Report", MB_OK | MB_ICONINFORMATION);

	}
	catch (CException* e)
	{
		MessageBox("Error during file processing", "Error", MB_OK | MB_ICONERROR);
		e->Delete();
		
	}
		
}

void CTeleshininaXorDecoderDlg::showFreqTable(vector<uint32_t> const &freqTable, vector<double> const &relFreqTable)
{
	m_sDecFreqLst.DeleteAllItems();
	CString tmp;
	for (size_t i = 0; i < freqTable.size(); ++i)
	{	
		if (freqTable[i])
		{
			tmp.Format("%s", CString((char)i));
			size_t nItem = m_sDecFreqLst.InsertItem(m_sDecFreqLst.GetItemCount(), tmp, -1);
			tmp.Format("%d", freqTable[i]);
			m_sDecFreqLst.SetItemText(nItem, 1, tmp);
			tmp.Format("%g", relFreqTable[i]);
			m_sDecFreqLst.SetItemText(nItem, 2, tmp);
		}
	}
}

void CTeleshininaXorDecoderDlg::showDecodedFreqTable(vector<uint8_t> decodedTable, vector<uint32_t> freqTable, vector<double> relFreqTable)
{
	CString tmp;
	size_t nItem = 0;
	for (int nItem = 0; nItem < m_sDecFreqLst.GetItemCount(); ++nItem)
	{
		string cur_char = ((LPCTSTR)m_sDecFreqLst.GetItemText(nItem, 0));
				
		uint8_t cur_char_num = cur_char[0];
		
		tmp.Format("%s", CString((char)decodedTable[cur_char_num]));
		//tmp.Format("%d", decodedTable[cur_char_num]);
		m_sDecFreqLst.SetItemText(nItem, 3, tmp);
		tmp.Format("%d", freqTable[decodedTable[cur_char_num]]);
		m_sDecFreqLst.SetItemText(nItem, 4, tmp);
		tmp.Format("%g", relFreqTable[decodedTable[cur_char_num]]);
		m_sDecFreqLst.SetItemText(nItem, 5, tmp);
			
		
	}
}

void CTeleshininaXorDecoderDlg::OnSavaDecoded()
{
	using namespace decoder_nmsp;
	if (m_sFileout == "")
	{
		MessageBox("Please chouse output text file", "Error", MB_OK | MB_ICONERROR);
		return;
	}

	try
	{
		decoder.writeOutputFile();
		//CString text = decoder.getDecodedText().substr(0, 500).c_str();
		//MessageBox(text);
		//m_sDecodedText = text;
		//UpdateData(FALSE);

	}
	catch (CException* e)
	{
		MessageBox(_T("Error reading file"));
		e->Delete();
	}

}


void CTeleshininaXorDecoderDlg::OnSaveTable()
{
	using namespace decoder_nmsp;
	
	CFileDialog dlg(FALSE);
	if (dlg.DoModal() == IDOK)
	{
		m_sTable = dlg.m_ofn.lpstrFile;
		UpdateData(FALSE);

		decoder.selectInputTablePath((LPCTSTR)m_sTable);

		try
		{

			if (decoder.writeTableToFile())
			{
				MessageBox("Successful table saved", "Report", MB_OK | MB_ICONINFORMATION);
			}
			else
			{
				MessageBox("Error during table saving", "Error", MB_OK | MB_ICONERROR);
			}

		}
		catch (CException* e)
		{
			MessageBox("Error during table saving", "Error", MB_OK | MB_ICONERROR);
			e->Delete();
		}
	}
}


void CTeleshininaXorDecoderDlg::OnBtnEditTable()
{
	
	if (m_sDecFreqLst.GetSelectedCount() == 0)
	{
		MessageBox("Please chouse the row for edit", "Edit", MB_OK | MB_ICONINFORMATION);
	}

	
	POSITION pos = m_sDecFreqLst.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		CEditDlg dlg;
		
		
		using namespace decoder_nmsp;
		int nItem = m_sDecFreqLst.GetNextSelectedItem(pos);

		CString tmp;
		
		
		string cur_char = ((LPCTSTR)m_sDecFreqLst.GetItemText(nItem, 0));
		uint8_t cur_char_num = cur_char[0];
		uint8_t prev_enc_char_num = cur_char_num;
		uint8_t prev_dec_char_num = decoder.getCodeToOriginal()[cur_char_num];
		

		tmp.Format("%s", CString(cur_char[0]));
		dlg.m_sEncChar = tmp;
		
		cur_char = ((LPCTSTR)m_sDecFreqLst.GetItemText(nItem, 3)); 
		tmp.Format("%s", CString(cur_char[0]));
		dlg.m_sDecChar = tmp;
		
		if (dlg.DoModal() == IDOK)
		{
			string cur_enc_char = ((LPCTSTR)dlg.m_sEncChar);
			uint8_t cur_enc_char_num = cur_enc_char[0];

			string cur_dec_char = ((LPCTSTR)dlg.m_sDecChar);
			uint8_t cur_dec_char_num = cur_dec_char[0];
			
			vector<uint8_t> &table = decoder.getCodeToOriginalToEdit();
			if (cur_enc_char_num != prev_enc_char_num)
			{
				table[prev_enc_char_num] = 0;
			}
			

			for (size_t i = 0; i < table.size(); ++i)
			{
				if (table[i] == cur_dec_char_num)
				{
					table[i] = prev_dec_char_num;
				}
			}

			table[cur_enc_char_num] = cur_dec_char_num;
			showFreqTable(decoder.getFreqTableSourceV(), decoder.getRelFreqTableSourceV());
			showDecodedFreqTable(decoder.getCodeToOriginal(), decoder.getFreqTableOriginalV(), decoder.getRelFreqTableOriginalV());

			
			/*m_sDecFreqLst.SetItemText(nItem, 0, dlg.m_sEncChar);
			m_sDecFreqLst.SetItemText(nItem, 1, "0");
		    m_sDecFreqLst.SetItemText(nItem, 2, "0");
			m_sDecFreqLst.SetItemText(nItem, 3, dlg.m_sDecChar);
			m_sDecFreqLst.SetItemText(nItem, 4, "0");
			m_sDecFreqLst.SetItemText(nItem, 5, "0");*/
		}
			
	}

	OnBtnReprocess();
}


void CTeleshininaXorDecoderDlg::OnBtnLoadTable()
{
	using namespace decoder_nmsp;

	CFileDialog dlg(TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_sTable = dlg.m_ofn.lpstrFile;
		UpdateData(FALSE);
		decoder.selectInputTablePath((LPCTSTR)m_sTable);
		try
		{

			if (decoder.readTableFromFile())
			{
				showFreqTable(decoder.getFreqTableSourceV(), decoder.getRelFreqTableSourceV());
				showDecodedFreqTable(decoder.getCodeToOriginal(), decoder.getFreqTableOriginalV(), decoder.getRelFreqTableOriginalV());
				UpdateData(FALSE);

				MessageBox("Successful table loaded", "Report", MB_OK | MB_ICONINFORMATION);
			}
			else
			{
				MessageBox("Error during table loading", "Error", MB_OK | MB_ICONERROR);
			}

		}
		catch (CException* e)
		{
			MessageBox("Error during table loading", "Error", MB_OK | MB_ICONERROR);
			e->Delete();
		}
	}
}


void CTeleshininaXorDecoderDlg::OnBtnReprocess()
{
	if (m_sFilein == "" || m_sTable == "")
	{
		MessageBox("Please chouse source text file and table", "Error", MB_OK | MB_ICONERROR);
		return;
	}


	using namespace decoder_nmsp;
	
	if (!decoder.getCodeToOriginal().size())
	{
		MessageBox("Table is empty", "Error", MB_OK | MB_ICONERROR);
		return;
	}
		
	try
	{
		decoder.decodeWithTable();

		CString text = decoder.getDecodedText().substr(0, 500).c_str();
		//MessageBox(text);
		m_sDecodedText = text;
		UpdateData(FALSE);
		MessageBox("Successful processing done", "Report", MB_OK | MB_ICONINFORMATION);

	}
	catch (CException* e)
	{
		MessageBox("Error during file processing", "Error", MB_OK | MB_ICONERROR);
		e->Delete();

	}
}
