#include "XOR.h"

XOR::XOR(){
	outtext = "";
}

XOR::XOR(const string& filein, const string& fileout){
	XOR(filein, fileout, "");
}

XOR::XOR(const string& filein, const string& fileout, const string& key){
	selectInputFile(filein);
	selectOutputFile(fileout);
	this->key = key;
}

XOR::~XOR(){
	fin.close();
	fout.close();
}

bool XOR::selectInputFile(const string& filename){
	fin.open(filename,ios::binary);
	return fin.is_open();
}

bool XOR::selectOutputFile(const string& filename){
	fout.open(filename,ios::binary);
	return fout.is_open();
}

void XOR::selectKey(const string& key){
	this->key = key;
}

void XOR::readFromFileAndEncrypt(){
	int j = 0;
	int currentChar;
	outtext.clear();
	while (true)
	{
		currentChar = fin.get();
		if (fin.eof()) break;
		if (j == key.length())
		{
			j = 0;
		}
		char cur_key_char = key[j++];
		char cur_encoded_char = currentChar ^ cur_key_char;
		outtext += cur_encoded_char;
		fout.put(cur_encoded_char);
	}
}

void XOR::writeToFile(){
	//fout << outtext.c_str();
}
