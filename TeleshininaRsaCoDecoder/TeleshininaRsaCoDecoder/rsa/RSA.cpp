#include "RSA.h"   
#include <time.h>   
#include <cstdint>
using namespace std;   
//***************************************************************   
// Constructors   
//***************************************************************   
// if bitsNr is little then 64 the bitNr is automaticly set on 64   
// if privateKeyFile or publickKeyFile is Empty String the key are automaticaly Generated;   

void RSA::GenKeysToFile(const std::string& privateKeyFile, const std::string& publicKeyFile, long bitsNr)
{
	if (bitsNr > 64)
		m_lBitsNr = bitsNr;
	else
		m_lBitsNr = 64;
	GenerateKeys();


	ofstream ofs(privateKeyFile);
	ofs << m_privateKey.D << ' ';
	ofs << m_privateKey.N << ' ';

	ofstream ofs1(publicKeyFile);
	ofs1 << m_publicKey.E << ' ';
	ofs1 << m_publicKey.N << ' ';


	ofs.close();
	ofs1.close();
}

RSA::RSA(const std::string& filein, const std::string& fileout, const std::string& privateKeyFile, const std::string& publicKeyFile, long bitsNr)
{   
	selectInputFile(filein);
	selectOutputFile(fileout);
	if (!LoadKeys(publicKeyFile, privateKeyFile))
	{
		GenKeysToFile(privateKeyFile, publicKeyFile, bitsNr);
	}
	
}   
   
RSA::~RSA(void)   
{   
}   
   

void RSA::selectInputFile(const string& filename){
	finName = filename;
}

void RSA::selectOutputFile(const string& filename){
	foutName = filename;
}


//***************************************************************   
// Utils   
//***************************************************************   
// Generate   
//  P,Q  - large prime number   
//  N = P*Q   
//  E,D from FI(N), E*D congr 1 mod FI(N) / E small but D > (1/3)*N^(1/4)   
//                  eqv with D represented on at least ((nrB + 1)/4) + 1 bits   
void RSA::GenerateKeys()   
{   
    time_t t;   
    time( &t );   
    NTL::SetSeed(NTL::to_ZZ((unsigned long)(t + 15)));    
   
    // P, Q   
	NTL::ZZ P, Q;
    NTL::GenPrime(P, m_lBitsNr, 100);   
    do   
    {   
        NTL::GenPrime(Q, m_lBitsNr, 100);   
    }   
    while (Q < P && P < 2* Q);   
   
    // N   
    NTL::ZZ phiN;   
    NTL::mul(m_publicKey.N, P, Q);
	NTL::mul(m_privateKey.N, P, Q);
   
	//phiN
    NTL::mul(phiN, P - 1, Q - 1);   
    
	//D   
    do    
    {   
        NTL::RandomPrime(m_privateKey.D, m_lBitsNr - 32, 100);   
    }   
    while (NTL::divide(phiN, m_privateKey.D) == 1);   
   
    if ( NTL::InvModStatus(m_publicKey.E, m_privateKey.D, phiN) != 0) //error InvMod doesn't exists   
        throw "InvMod of D doesn't exists";   
}   
   
bool RSA::LoadKeys(std::string publicKeyFile,std::string privateKeyFile)   
{   
    ifstream ifsPublic(publicKeyFile.c_str());   
    ifstream ifsPrivate(privateKeyFile.c_str());   
    if (ifsPublic.is_open())   
    {   
		ifsPublic >> m_publicKey.E;
		ifsPublic >> m_publicKey.N;
    }   
    if (ifsPrivate.is_open())   
    {   
		ifsPrivate >> m_privateKey.D;
		ifsPrivate >> m_privateKey.N;
    }   
            
    ifsPrivate.close();   
    ifsPublic.close();   
	return true;
}   
//***************************************************************   
// Get Methods   
//***************************************************************   
RSA::PublicKey RSA::GetPublicKey() const   
{   
    return m_publicKey;   
}   
   
//***************************************************************   
// Crypto Function   
//***************************************************************   
   
void RSA::Encode(const NTL::ZZ &x, NTL::ZZ &y, const RSA::PublicKey &key)   
{   
    if (x > key.N)   
        throw "Plain Text must be from Z_N";   
    NTL::PowerMod(y, x, key.E, key.N);   
}   
   
void RSA::Decode(const NTL::ZZ &y, NTL::ZZ &x)
{
	// Clasic Decoding   
	NTL::PowerMod(x, y, m_privateKey.D, m_privateKey.N);
	
}


void RSA::EncodeFromFile(){
	

	ifstream fin(finName, ios::binary);
	//ofstream fout(foutName, ios::binary);
	ofstream fout(foutName);
	
	uint8_t prevChar = 0;
	uint8_t currentChar;
	NTL::ZZ currentCodeZZ;
	NTL::ZZ currentCharZZ;
	while (true)
	{
		currentChar = fin.get();
		if (fin.eof()) break;
		//cout << currentChar;

		uint8_t curShiftChar = prevChar + currentChar;
		//cout << " : " << (int)curShiftChar;
		currentCharZZ = NTL::to_ZZ((unsigned long)(curShiftChar));

		Encode(currentCharZZ, currentCodeZZ, GetPublicKey());

		
		//cout << " : " << currentCodeZZ << endl << endl;
		fout << currentCodeZZ << ' ';
		prevChar = currentChar;

		
	}
	fin.close();
	fout.close();
}

void RSA::DecodeFromFile(){

	//ifstream fin(finName, ios::binary);
	ifstream fin(finName);
	ofstream fout(foutName, ios::binary);

	uint8_t prevChar = 0;
	uint8_t currentCode;
	NTL::ZZ currentCodeZZ;
	NTL::ZZ currentCharZZ;
	while (true)
	{
		fin >> currentCodeZZ;
		if (fin.eof()) break;
				
		//cout << currentCodeZZ;
		Decode(currentCodeZZ, currentCharZZ);

		uint8_t curEncodedChar = NTL::to_int(currentCharZZ);
		//cout << " : " << int(curEncodedChar);

		curEncodedChar -= prevChar ;
		//cout << " : " << (int)curEncodedChar << endl << endl;
		//cout << curEncodedChar;
		fout.put(curEncodedChar);
		prevChar = curEncodedChar;
	}
	fin.close();
	fout.close();
}

void RSA::PrintKeys()
{
	cout << "private key: " << endl;	
	cout << "D: " << m_privateKey.D << endl << endl;
	cout << "N: " << m_privateKey.N << endl << endl;

	cout << "public key: " << endl;	
	cout << "E: " << m_publicKey.E << endl << endl;
	cout << "N: " << m_publicKey.N << endl << endl;
	cout << endl;
}