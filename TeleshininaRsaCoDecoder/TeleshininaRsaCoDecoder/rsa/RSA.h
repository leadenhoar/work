#pragma once 
//#pragma comment (lib, "//NTL/Debug/NTL.lib") 
 
#include "NTL/ZZ.h" 
#include <string> 
#include <fstream> 
 
class RSA 
{ 
public: 
	typedef struct _publicKey 
	{ 
		NTL::ZZ N; //N = p*q 
		NTL::ZZ E; // e*d congr 1 mod Phi(n) = (p-1)*(q-1); e is ususly small but wrt d 
	} PublicKey; 
 
	typedef struct _privateKey 
	{ 
		NTL::ZZ N; // N = p*q 
		NTL::ZZ D; // d > (1/3)*n^(1/4); d from Z*_Phi(N) = (p - 1)*(q - 1) 
	} PrivateKey; 
 
private: 
	PublicKey m_publicKey; 
	PrivateKey m_privateKey;
	
	std::string finName;
	std::string foutName;
 
private: 
	long m_lBitsNr; // >=64
	 
private: 
	void GenerateKeys(); 
 
	bool LoadKeys(std::string publicKeyFile,std::string privateKeyFile); 
public: 
	RSA(const std::string& filein = "", const std::string& fileout = "", const std::string& privateKeyFile = "", const std::string& publicKeyFile = "", long bitsNr = 0);
	~RSA(void); 
 
public: 
	PublicKey GetPublicKey() const; 
public: 
	// Encode number x with public key (n,e); 
	// The result will be put in y; 
	void Encode(const NTL::ZZ& x, NTL::ZZ& y, const PublicKey& key); 
	// Decode y with my private key (m_zzP, m_zzQ, m_zzD) 
	// The result will be put in x 
	void Decode(const NTL::ZZ&y, NTL::ZZ& x);
	

public:
	void selectInputFile(const std::string&);
	void selectOutputFile(const std::string&);
	void EncodeFromFile();
	void DecodeFromFile();
	void GenKeysToFile(const std::string& privateKeyFile, const std::string& publicKeyFile, long bitsNr);
	void PrintKeys();
}; 