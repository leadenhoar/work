#include "RSA.h"   
#include "Wiener.h"   
#include <time.h>
#include <iostream>   
#include <string>   
#include <string>   
#include <windows.h>   
using namespace std;   



void testRsa()
{
	try
	{

		RSA Bob("", "", "BobPrvKey.txt", "BobPubKey.txt", 512);

		NTL::ZZ x;
		NTL::ZZ y;
		NTL::ZZ z;
		x = 12345;

		cout << x << endl;
		Bob.Encode(x, y, Bob.GetPublicKey());
		cout << y << endl;
		Bob.Decode(y, z);
		cout << z << endl;
	}
	catch (char s[100])
	{
		cout << s << endl;
	}

}
void encode()   
{   
	try
	{
		clock_t t1 = clock();
		cout << "Encoding: " << endl;
		string
			filename_in = "Orig.txt",
			filename_out = "Encoded.txt",
			private_key = "TeleshininaPrvKey.txt",
			pubic_key = "TeleshininaPubKey.txt";

		int bits = 512;

		RSA EncodeCryptoSystem(filename_in, filename_out, private_key, pubic_key, bits);
		//EncodeCryptoSystem.PrintKeys();
		EncodeCryptoSystem.EncodeFromFile();
				
		clock_t t2 = clock();
		cout << endl << endl << "Encode time: " << double(t2 - t1) / CLOCKS_PER_SEC<< "sec" << endl << endl;
	}
	catch (char s[100])
	{
		cout << s << endl;
	}
}

void decode()
{
	try
	{
		cout << "Decoding: " << endl;
		clock_t t1 = clock();
		string
			filename_in = "Encoded.txt",
			filename_out = "Decoded.txt",
			private_key = "TeleshininaPrvKey.txt",
			pubic_key = "TeleshininaPubKey.txt";

		
		int bits = 512;

		RSA DecodeCryptoSystem(filename_in, filename_out, private_key, pubic_key, bits);
		//DecodeCryptoSystem.PrintKeys();
		DecodeCryptoSystem.DecodeFromFile();

		clock_t t2 = clock();
		cout << endl << endl << "Decode time: " << double(t2 - t1) / CLOCKS_PER_SEC << "sec" << endl << endl;

    }   
    catch(char s[100])   
    {   
        cout << s << endl;   
    }   
    
}  

void menu()
{
	
	

	int menu;
	do{
		cout << endl;
		cout << "Menu:" << endl;
		cout << "1: Encode" << endl;
		cout << "2: Decode" << endl;
		cout << "3: Gen keys" << endl;
		cout << "9: Clear sreen" << endl;
		cout << "0: Exit" << endl;
		cin >> menu;
		
		switch (menu){
		case 1:
		{

			string
				origin_filename = "Origin.txt",
				encode_filename = "Encoded.txt",
				decode_filename = "Decoded.txt",
				private_key = "TeleshininaPrvKey.txt",
				pubic_key = "TeleshininaPubKey.txt";
			long bits = 64;

			string tmp;
			cout << endl;
			cout << "Encoding: " << endl;
			cout << "\tOriginal file name" << endl << "\tOrigin.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n") origin_filename = tmp;
			cout << endl;

			cout << "\tEncoded file name" << endl << "\tEncoded.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n")  encode_filename = tmp;
			cout << endl;

			cout << "\tPublic key file name" << endl << "\tTeleshininaPubKey.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n")  pubic_key = tmp;
			cout << endl;

			ifstream ifsPublic(pubic_key);
			if (!ifsPublic.is_open())
			{
				ifsPublic.close();
				cout << "The Public Key File couldn't be open. The Keys were automaticaly generated" << endl;
				
				/*cout << "\tKey len" << endl << "\t64 if it is [n]: ";
				cin >> tmp;
				std::string::size_type sz;
				if (tmp != "n") bits = stol(tmp, &sz);
				cout << endl;*/


				cout << "\tPrivate key file name" << endl << "\tTeleshininaPrvKey.txt if it is [n]: ";
				cin >> tmp;
				if (tmp != "n")  private_key = tmp;
				cout << endl;

				RSA EncodeCryptoSystem(origin_filename, encode_filename, private_key, pubic_key, bits);
				EncodeCryptoSystem.GenKeysToFile(private_key, pubic_key, bits);
				cout << endl;
				cout << "\tWith printing keys [p]" << endl;
				cout << "\tWithout printing press any key: ";
				cin >> tmp;
				if (tmp == "p")
				{
					EncodeCryptoSystem.PrintKeys();
				}
				cout << endl;

				clock_t t1 = clock();
				EncodeCryptoSystem.EncodeFromFile();
				clock_t t2 = clock();

				cout << endl << endl << "Encode time: " << double(t2 - t1) / CLOCKS_PER_SEC << "sec" << endl << endl;
			}
			else
			{
				ifsPublic.close();
				RSA EncodeCryptoSystem(origin_filename, encode_filename, "", pubic_key, bits);
				cout << endl;
				cout << "\tWith printing keys [p]" << endl;
				cout << "\tWithout printing press any key: ";
				cin >> tmp;
				if (tmp == "p")
				{
					EncodeCryptoSystem.PrintKeys();
				}
				cout << endl;

				clock_t t1 = clock();
				EncodeCryptoSystem.EncodeFromFile();
				clock_t t2 = clock();

				cout << endl << endl << "Encode time: " << double(t2 - t1) / CLOCKS_PER_SEC << "sec" << endl << endl;
			}
			
			system("pause");
		}

			break;


		case 2:
		{
			string
				origin_filename = "Origin.txt",
				encode_filename = "Encoded.txt",
				decode_filename = "Decoded.txt",
				private_key = "TeleshininaPrvKey.txt",
				pubic_key = "TeleshininaPubKey.txt";
			long bits = 64;

			string tmp;
			cout << endl;
			cout << "Decoding: " << endl;
			cout << "\tEncoded file name" << endl << "\tEncoded.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n") encode_filename = tmp;
			cout << endl;

			cout << "\tDecoded file name" << endl << "\tDecoded.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n")  decode_filename = tmp;
			cout << endl;

			cout << "\tPrivate key file name" << endl << "\tTeleshininaPrvKey.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n")  private_key = tmp;
			cout << endl;

			ifstream ifsPrivate(private_key);
			if (!ifsPrivate.is_open())
			{
				cout << "Private key file not exist, decoding aborted." << endl;
				ifsPrivate.close();
				break;
			}
			ifsPrivate.close();
			RSA EncodeCryptoSystem(encode_filename, decode_filename, private_key, "", bits);

			cout << endl;
			cout << "\tWith printing keys: p" << endl;
			cout << "\tWithout printing press any key: ";
			cin >> tmp;
			if (tmp == "p")
			{
				EncodeCryptoSystem.PrintKeys();
			}
			cout << endl;

			clock_t t1 = clock();
			EncodeCryptoSystem.DecodeFromFile();
			clock_t t2 = clock();

			cout << endl << endl << "Decode time: " << double(t2 - t1) / CLOCKS_PER_SEC << "sec" << endl << endl;
			system("pause");
		}
			break;
		
		case 3:
		{
			string
				origin_filename = "Origin.txt",
				encode_filename = "Encoded.txt",
				decode_filename = "Decoded.txt",
				private_key = "TeleshininaPrvKey.txt",
				pubic_key = "TeleshininaPubKey.txt";
			long bits = 64;

			string tmp;
			cout << endl;
			cout << "Keys gen: " << endl;
				  
			/*cout << "\tKey len" << endl << "\t64 if it is [n]: ";
			cin >> tmp;
			std::string::size_type sz;
			if (tmp!="n") bits = stol(tmp, &sz);
			cout << endl;*/

			cout << "\tPrivate key file name" << endl << "\tTeleshininaPrvKey.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n")  private_key = tmp;
			cout << endl;

			cout << "\tPublic key file name" << endl << "\tTeleshininaPubKey.txt if it is [n]: ";
			cin >> tmp;
			if (tmp != "n")  pubic_key = tmp;
			cout << endl;

			ofstream ftmp(private_key);
			ftmp.clear();
			ftmp.close();

			ftmp.open(pubic_key);
			ftmp.clear();
			ftmp.close();
				  
			RSA EncodeCryptoSystem("", "", "", "", 0);
			EncodeCryptoSystem.GenKeysToFile(private_key, pubic_key, bits);

			cout << endl;
			cout << "\tWith printing keys [p]" << endl;
			cout << "\tWithout printing press any key: ";
			cin >> tmp;
			if (tmp == "p")
			{
				EncodeCryptoSystem.PrintKeys();
			}
			cout << endl;

			system("pause");
		}

			break;

		case 9:
			system("cls");
			break;
		}
	} while (menu != 0);

}

int main()
{
	menu();
	return 0;
}
