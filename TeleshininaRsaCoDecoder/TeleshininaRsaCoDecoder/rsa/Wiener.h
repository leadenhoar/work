#pragma once 
#include "RSA.h" 
 
#include <iostream> 
class Wiener 
{ 
public: 
	typedef struct _param 
	{ 
		NTL::ZZ D; 
		NTL::ZZ PhiN; 
		NTL::ZZ Q; 
		NTL::ZZ P; 
		bool Succeded; 
	} WienerResult; 
private: 
	RSA::PublicKey m_rsaKey; 
	 
	long m_nK; 
	WienerResult m_result; 
 
private: 
	void Calculate(); 
	bool Criterion(const NTL::ZZ& L, const NTL::ZZ& D); 
public: 
	Wiener(); 
	~Wiener(void); 
public: 
	WienerResult Atack(const RSA::PublicKey& key); 
	void PrintAtackResults() const; 
}; 