#pragma once
#include "stdafx.h"
#include <SDKDDKVer.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <afx.h>
#include <cctype>
#include <vector>
#include <iterator>
#include <memory>




using namespace std;
class StudentTeleshinina
	: public CObject
{
public:
	
	//DECLARE_DYNAMIC(StudentTeleshinina)
	StudentTeleshinina();
	virtual ~StudentTeleshinina();
	virtual void ReadData(CString s_name, CString name, CString surname, size_t age, double point);
	virtual CString WriteData();
	virtual void Serialize(CArchive &archive);
	CString getName();
	CString getSName();
	CString getSurName();
	size_t  getAge();
	double  getPoint();

	void setName(CString name);
	void setSName(CString s_name);
	void setSurName(CString sur_name);
	void setAge(size_t age);
	void setPoint(double point);
	DECLARE_SERIAL(StudentTeleshinina)
private:
	CString s_name_;
	CString name_;
	CString surname_;
	size_t  age_;
	double  point_;
};

class CaptainTeleshinina
	: public StudentTeleshinina
{
private:
	size_t id_;

public:
	//DECLARE_DYNAMIC(CaptainTeleshinina)
	CaptainTeleshinina();
	virtual ~CaptainTeleshinina();
	virtual void ReadData(CString s_name, CString name, CString surname, size_t age, double point, size_t id);
	virtual CString WriteData();
	virtual void Serialize(CArchive &archive);
	size_t  getId();
	void    setId(size_t id);
	DECLARE_SERIAL(CaptainTeleshinina)
};

class Group {
private:
	std::vector<std::shared_ptr<StudentTeleshinina>> students;
	
public:
	Group();
	~Group();
	void AddStudentTeleshinina(CString s_name, CString name, CString surname, size_t age, double point);
	void AddCaptainTeleshinina(CString s_name, CString name, CString surname, size_t age, double point, size_t id);
	CString Print();
	bool ReadFile(std::string filename);
	bool WriteFile(std::string filename);
	void Serialize(CArchive &archive);
	void deleteElem(size_t position);
	std::vector<std::shared_ptr<StudentTeleshinina>> *GetStudents();
	void Clear();
	CSize GetTextSize();
};

