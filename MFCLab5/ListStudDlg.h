#pragma once
#include "afxcmn.h"
#include "ContElems.h"

// CListStudDlg dialog

class CListStudDlg : public CDialog
{
	DECLARE_DYNAMIC(CListStudDlg)

public:
	CListStudDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CListStudDlg();

// Dialog Data
	enum { IDD = IDD_DLG_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_StudList;
	afx_msg void OnBnClickedBtnEdit();
	Group *group;
	void FillList();
	afx_msg void OnBnClickedBtnAdd();
	afx_msg void OnBnClickedBtnDel();
};
