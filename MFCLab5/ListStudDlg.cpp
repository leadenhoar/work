// ListStudDlg.cpp : implementation file
//
#pragma once
#include "ContElems.h"
#include "stdafx.h"
#include "MFCLab4.h"
#include "ListStudDlg.h"
#include "afxdialogex.h"
#include "StudAddDlg.h"


// CListStudDlg dialog

IMPLEMENT_DYNAMIC(CListStudDlg, CDialog)

CListStudDlg::CListStudDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CListStudDlg::IDD, pParent)
{

}

CListStudDlg::~CListStudDlg()
{
}

void CListStudDlg::FillList()
{
	m_StudList.DeleteAllItems();
	std::vector<std::shared_ptr<StudentTeleshinina>> *students = group->GetStudents();
	for (size_t sti = 0; sti < students->size(); ++sti)
	{
		StudentTeleshinina *student = (*students)[sti].get();
		CaptainTeleshinina *captain = dynamic_cast<CaptainTeleshinina *>(student);

		int nItem;
		if (captain)
		{
			nItem = m_StudList.InsertItem(m_StudList.GetItemCount(), "Captain", -1);
		}
		else
		{
			nItem = m_StudList.InsertItem(m_StudList.GetItemCount(), "Student", -1);
		}

		m_StudList.SetItemText(nItem, 1, student->getName());
	}
}

void CListStudDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_STUD, m_StudList);

	m_StudList.InsertColumn(0, "Type", LVCFMT_LEFT, 100);
	m_StudList.InsertColumn(1, "Name", LVCFMT_LEFT, 100);

	FillList();

}


BEGIN_MESSAGE_MAP(CListStudDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_EDIT, &CListStudDlg::OnBnClickedBtnEdit)
	ON_BN_CLICKED(IDC_BTN_ADD, &CListStudDlg::OnBnClickedBtnAdd)
	ON_BN_CLICKED(IDC_BTN_DEL, &CListStudDlg::OnBnClickedBtnDel)
END_MESSAGE_MAP()


// CListStudDlg message handlers


void CListStudDlg::OnBnClickedBtnEdit()
{
	
	if (m_StudList.GetSelectedCount() == 0)
	{
		MessageBox("Please chouse the row for edit", "Edit", MB_OK | MB_ICONINFORMATION);
	}


	POSITION pos = m_StudList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nItem = m_StudList.GetNextSelectedItem(pos);

		CString type = m_StudList.GetItemText(nItem, 0);
		CString name = m_StudList.GetItemText(nItem, 1);
		
		CStudAddDlg dlg;

		std::vector<std::shared_ptr<StudentTeleshinina>> *students = group->GetStudents();
		StudentTeleshinina *student = (*students)[nItem].get();
		CaptainTeleshinina *captain = dynamic_cast<CaptainTeleshinina *>(student);

		dlg.m_sSName = (*students)[nItem]->getSName();
		dlg.m_sName = (*students)[nItem]->getName();
		dlg.m_sSurName = (*students)[nItem]->getSurName();
		dlg.m_iAge = (*students)[nItem]->getAge();
		dlg.m_dPoint = (*students)[nItem]->getPoint();

		if (captain)
		{
			dlg.m_iId = captain->getId();
			dlg.edit = true;
		}
		else
		{
			dlg.edit = true;
			dlg.m_iId = -1;
		}
			
		if (IDOK == dlg.DoModal())
		{
			student->setSName(dlg.m_sSName);
			student->setName(dlg.m_sName);
			student->setSurName(dlg.m_sSurName),
			student->setAge(dlg.m_iAge);
			student->setPoint(dlg.m_dPoint);
			
			if (captain)
			{
				captain->setId(dlg.m_iId);
			}
			else
			{
				
			}

		}

		FillList();

		nItem = (nItem < (m_StudList.GetItemCount() - 1) ? nItem : (m_StudList.GetItemCount() - 1));
		m_StudList.SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
		m_StudList.EnsureVisible(nItem, FALSE);
		m_StudList.SetFocus();

	}	
}


void CListStudDlg::OnBnClickedBtnAdd()
{
	std::vector<std::shared_ptr<StudentTeleshinina>> *students = group->GetStudents();

	CStudAddDlg dlg;

	dlg.m_sSName = "Doe";
	dlg.m_sName = "John";
	dlg.m_sSurName = "Smith";
	dlg.m_iAge = 21;
	dlg.m_dPoint = 5.;
	dlg.m_iId = 0;
	dlg.edit = false;
	if (IDOK == dlg.DoModal())
	{

		if (!dlg.m_eRdoGrp)
		{
			group->AddStudentTeleshinina(
				dlg.m_sSName, dlg.m_sName, dlg.m_sSurName,
				dlg.m_iAge, dlg.m_dPoint);
		}

		if (dlg.m_eRdoGrp)
		{

			group->AddCaptainTeleshinina(
				dlg.m_sSName, dlg.m_sName, dlg.m_sSurName,
				dlg.m_iAge, dlg.m_dPoint, dlg.m_iId);
		}

	}
	FillList();
	int nItem = m_StudList.GetItemCount() - 1;
	m_StudList.SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
	m_StudList.EnsureVisible(nItem, FALSE);
	m_StudList.SetFocus();
}


void CListStudDlg::OnBnClickedBtnDel()
{

	if (m_StudList.GetSelectedCount() == 0)
	{
		MessageBox("Please chouse the row for delete", "Delete", MB_OK | MB_ICONINFORMATION);
	}


	POSITION pos = m_StudList.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nItem = m_StudList.GetNextSelectedItem(pos);
		group->deleteElem(nItem);
		
		FillList();
		nItem = (nItem < (m_StudList.GetItemCount() - 1) ? nItem : (m_StudList.GetItemCount() - 1));
		m_StudList.SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
		m_StudList.EnsureVisible(nItem, FALSE);
		m_StudList.SetFocus();
		
	}
}
