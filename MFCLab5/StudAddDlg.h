#pragma once


// CStudAddDlg dialog

class CStudAddDlg : public CDialog
{
	DECLARE_DYNAMIC(CStudAddDlg)

public:
	CStudAddDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CStudAddDlg();

// Dialog Data
	enum { IDD = IDD_ADD_STUD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_sSName;
	CString m_sName;
	CString m_sSurName;
	int m_iAge;
	double m_dPoint;
	int m_iId;
	bool edit;
//	BOOL m_bStudent;
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
//	BOOL m_bCaptain;
	int m_eRdoGrp;
	afx_msg void OnBnClickedStudent();
	afx_msg void OnBnClickedCaptain();
	void DoEnabling();
	afx_msg void OnEnChangeEditSurName();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnSetfocusStudent();
	afx_msg void OnBnSetfocusCaptain();
};
