
// MFCLab4.h : main header file for the MFCLab4 application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols



// CLab4App:
// See MFCLab4.cpp for the implementation of this class
//

class CLab4App : public CWinApp
{
public:
	CLab4App();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CLab4App theApp;
