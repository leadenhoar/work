
// Lab4View.h : interface of the CLab4View class
//

#pragma once


class CLab4View : public CScrollView 
{
protected: // create from serialization only
	CLab4View();
	DECLARE_DYNCREATE(CLab4View)

// Attributes
public:
	CLab4Doc* GetDocument() const;

// Operations
public:

// Overrides
public:
	
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CLab4View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void OnDraw(CDC* pDC);		// overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	void fText(CPaintDC & dc);
	afx_msg void OnPaint(CDC* pDC);
	afx_msg void OnAdd();
	void PrintGroup(CDC* pDC);
	afx_msg void OnModifyPrint();
	afx_msg void OnModifyClear();
	afx_msg void OnModifyList();
};

#ifndef _DEBUG  // debug version in Lab4View.cpp
inline CLab4Doc* CLab4View::GetDocument() const
   { return reinterpret_cast<CLab4Doc*>(m_pDocument); }
#endif

