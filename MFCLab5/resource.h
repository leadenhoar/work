//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MFCLab4.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_Lab4TYPE                    130
#define IDD_ADD_STUD                    311
#define IDD_DLG_LIST                    312
#define IDC_EDIT_SEC_NAME               1000
#define IDC_EDIT_NAME                   1001
#define IDC_EDIT_SUR_NAME               1002
#define IDC_EDIT_AGE                    1003
#define IDC_EDIT_POINT                  1004
#define IDC_EDIT_ID                     1005
#define IDC_STUDENT                     1006
#define IDC_CAPTAIN                     1007
#define IDC_STATIC_ID                   1008
#define IDC_STATIC_SUR_NAME             1008
#define IDC_STATIC__ID                  1010
#define IDC_LIST_STUD                   1011
#define IDC_BTN_EDIT                    1013
#define IDC_BTN_ADD                     1014
#define IDC_BTN_DEL                     1015
#define ID_MODIFY_ADD                   32771
#define ID_MODIFY_PRINT                 32772
#define ID_MODIFY_CLEAR                 32773
#define ID_Delt                         32783
#define ID_BUTTON32783                  32783
#define ID_MODIFY_LIST                  32784
#define ID_BUTTON32787                  32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        313
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
