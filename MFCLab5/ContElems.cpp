#include "stdafx.h"
#include "ContElems.h"
#include <functional>
#include <algorithm>


IMPLEMENT_SERIAL(StudentTeleshinina, CObject, VERSIONABLE_SCHEMA | 2)
//IMPLEMENT_DYNAMIC(StudentTeleshinina, CObject)

using namespace std;

StudentTeleshinina::StudentTeleshinina()
{

}

StudentTeleshinina::~StudentTeleshinina()
{

}

void StudentTeleshinina::ReadData(
	CString s_name, CString name, CString surname, size_t age, double point)
{
	s_name_ = s_name;
	name_ = name;
	surname_ = surname;
	age_ = age;
	point_ = point;
}

CString StudentTeleshinina::WriteData()
{
	CString out;
	CString tmp;
	out += "Second name: " + s_name_ +"\n";
	out += "Name: " + name_ + "\n";
	out += "Surname: " + surname_ + "\n";
	tmp.Format("%d", age_);
	out += "Age: " + tmp + "\n";	
	tmp.Format("%g", point_);
	out += "Point: " + (tmp) + "\n";
	return out;
	
}

CString StudentTeleshinina::getName()
{
	return name_;
}

CString StudentTeleshinina::getSName()
{
	return s_name_;
}

CString StudentTeleshinina::getSurName()
{
	return surname_;
}

size_t  StudentTeleshinina::getAge()
{
	return age_;
}

double  StudentTeleshinina::getPoint()
{
	return point_;
}

void StudentTeleshinina::setName(CString name)
{
	name_ = name;
}

void StudentTeleshinina::setSName(CString s_name)
{
	s_name_ = s_name;
}
void StudentTeleshinina::setSurName(CString sur_name)
{
	surname_ = sur_name;
}
void StudentTeleshinina::setAge(size_t age)
{
	age_ = age;
}
void StudentTeleshinina::setPoint(double point)
{
	point_ = point;
}

void StudentTeleshinina::Serialize(CArchive &archive)
{
	CObject::Serialize(archive);
	if (archive.IsStoring())
		archive << s_name_ << name_ << surname_ << age_ << point_;
	else
		archive >> s_name_ >> name_ >> surname_ >> age_ >> point_;
}


//IMPLEMENT_DYNAMIC(CaptainTeleshinina, CObject)
IMPLEMENT_SERIAL(CaptainTeleshinina, StudentTeleshinina, VERSIONABLE_SCHEMA | 2)

CaptainTeleshinina::CaptainTeleshinina()
{

}

CaptainTeleshinina::~CaptainTeleshinina()
{

}

void CaptainTeleshinina::ReadData(CString s_name, CString name, CString surname, size_t age, double point, size_t id)
{
	StudentTeleshinina::ReadData(s_name, name, surname, age, point);
	id_ = id;
}

CString CaptainTeleshinina::WriteData()
{
	CString out;
	CString tmp;
	out += StudentTeleshinina::WriteData();
	tmp.Format("%d", id_);
	out += "Id: " + tmp + "\n";
	return out;
}

void CaptainTeleshinina::Serialize(CArchive &archive)
{
	StudentTeleshinina::Serialize(archive);
	
	if (archive.IsStoring())
		archive << id_;
	else
		archive >> id_;
}

size_t  CaptainTeleshinina::getId()
{
	return id_;
}

void CaptainTeleshinina::setId(size_t id)
{
	id_ = id;
}
Group::Group()
{
	
}

Group::~Group()
{
	
}

void Group::AddStudentTeleshinina(CString s_name, CString name, CString surname, size_t age, double point){
	
	shared_ptr<StudentTeleshinina> student = make_shared<StudentTeleshinina>();
	student->ReadData(s_name, name, surname, age, point);
	students.push_back(student);
	
	
}

void Group::AddCaptainTeleshinina(CString s_name, CString name, CString surname, size_t age, double point, size_t id){
	
	shared_ptr<CaptainTeleshinina> captain = make_shared<CaptainTeleshinina>();
	captain->ReadData(s_name, name, surname, age, point, id);
	students.push_back(captain);
	
}

void Group::deleteElem(size_t position)
{
	if (position < students.size())
	{
		students[position].reset();
		students.erase(students.begin() + position);
	}
}

CString Group::Print()
{
	CString out;
	for (auto sti = students.begin(); sti != students.end(); ++sti)
	{
		out += (*sti)->WriteData();
		out += "\n";
	}
	if (students.size() == 0){
		out = "Group is empty.";
	}
	return out;
}

CSize Group::GetTextSize()
{
	CSize sizeTotal;
	sizeTotal.cy = 17 * 6 * students.size();
    sizeTotal.cx = 300;
	return sizeTotal;
}
bool Group::ReadFile(string filename){
	
	Clear();
	CFile fin;
	if (!fin.Open(CString(filename.c_str()), CFile::modeRead))
		return 0;
	
	CArchive archive(&fin, CArchive::load);
	size_t size;
	archive >> size;
	
	StudentTeleshinina *student;
	for (size_t i = 0; i < size; ++i)
	{
		
		archive >> student;
		students.push_back(shared_ptr<StudentTeleshinina>(student));
	}
	
	
	archive.Close();
	fin.Close();
	return 1;
}

bool Group::WriteFile(string filename)
{
	CFile fout;
	if (!fout.Open(CString(filename.c_str()), CFile::modeWrite | CFile::modeCreate))
		return 0;

	CArchive archive(&fout, CArchive::store);	
	archive << students.size();
	
	for (size_t sti = 0; sti < students.size(); ++sti)
	{
		archive << (students[sti]).get();
	}

	
	archive.Close();
	fout.Close();
	return 1;
}

std::vector<std::shared_ptr<StudentTeleshinina>> *Group::GetStudents()
{
	return &students;
}

void SerializeOne(CArchive *ar, shared_ptr<StudentTeleshinina> &studentPtr)
{
	*ar << studentPtr.get();
}
void Group::Serialize(CArchive &ar)
{
	if (ar.IsStoring())
	{		
		ar << students.size();
		using std::placeholders::_1;
		//for (size_t sti = 0; sti < students.size(); ++sti)
		std::for_each(students.begin(), students.end(),
			std::bind(&SerializeOne, &ar, _1));
		
	}
	else
	{
		size_t size;
		ar >> size;

		StudentTeleshinina *student;
		for (size_t i = 0; i < size; ++i)
		{

			ar >> student;
			students.push_back(shared_ptr<StudentTeleshinina>(student));
		}
	}
}

void Group::Clear(){
	
	students.clear();
	
}