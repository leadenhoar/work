// StudAddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MFCLab4.h"
#include "StudAddDlg.h"
#include "afxdialogex.h"


// CStudAddDlg dialog

IMPLEMENT_DYNAMIC(CStudAddDlg, CDialog)

CStudAddDlg::CStudAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStudAddDlg::IDD, pParent)
	, m_sSName(_T(""))
	, m_sName(_T(""))
	, m_sSurName(_T(""))
	, m_iAge(0)
	, m_dPoint(0)
	, m_iId(0)
	, m_eRdoGrp(0)
{
	
}

CStudAddDlg::~CStudAddDlg()
{
}

void CStudAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SEC_NAME, m_sSName);
	DDX_Text(pDX, IDC_EDIT_NAME, m_sName);
	DDX_Text(pDX, IDC_EDIT_SUR_NAME, m_sSurName);
	DDX_Text(pDX, IDC_EDIT_AGE, m_iAge);
	DDV_MinMaxInt(pDX, m_iAge, 0, 100);
	DDX_Text(pDX, IDC_EDIT_POINT, m_dPoint);
	DDV_MinMaxDouble(pDX, m_dPoint, 0, 5);
	DDX_Text(pDX, IDC_EDIT_ID, m_iId);
	DDV_MinMaxInt(pDX, m_iId, -2, 999999);



	//  DDX_Radio(pDX, IDC_STUDENT, m_bStudent);
	//  DDX_Radio(pDX, IDC_CAPTAIN, m_bCaptain);
	DDX_Radio(pDX, IDC_STUDENT, m_eRdoGrp);
}


BEGIN_MESSAGE_MAP(CStudAddDlg, CDialog)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_STUDENT, &CStudAddDlg::OnBnClickedStudent)
	ON_BN_CLICKED(IDC_CAPTAIN, &CStudAddDlg::OnBnClickedCaptain)
	ON_BN_CLICKED(IDOK, &CStudAddDlg::OnBnClickedOk)
	ON_BN_SETFOCUS(IDC_STUDENT, &CStudAddDlg::OnBnSetfocusStudent)
	ON_BN_SETFOCUS(IDC_CAPTAIN, &CStudAddDlg::OnBnSetfocusCaptain)
END_MESSAGE_MAP()


// CStudAddDlg message handlers





void CStudAddDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);
	CButton* rBut;

	if (edit)
	{

		if (m_iId == -1)
		{
			CWnd* pCtrl = GetDlgItem(IDC_STUDENT);
			pCtrl->ShowWindow(SW_HIDE);
			pCtrl = GetDlgItem(IDC_CAPTAIN);
			pCtrl->ShowWindow(SW_HIDE);
			pCtrl = GetDlgItem(IDC_EDIT_ID);
			pCtrl->ShowWindow(SW_HIDE);
			pCtrl = GetDlgItem(IDC_STATIC__ID);
			pCtrl->ShowWindow(SW_HIDE);

		}
		else
		{
			CWnd* pCtrl = GetDlgItem(IDC_STUDENT);
			pCtrl->ShowWindow(SW_HIDE);
			pCtrl = GetDlgItem(IDC_CAPTAIN);
			pCtrl->ShowWindow(SW_HIDE);
			pCtrl = GetDlgItem(IDC_EDIT_ID);
			pCtrl->ShowWindow(SW_SHOW);
			pCtrl = GetDlgItem(IDC_STATIC__ID);
			pCtrl->ShowWindow(SW_SHOW);
			rBut = (CButton *)GetDlgItem(IDC_STUDENT);
		}
	}

	else

	{
		rBut = (CButton *)GetDlgItem(IDC_STUDENT);
		rBut->SetFocus();
		rBut->SetCheck(true);
	}
	
	

	// TODO: Add your message handler code here
}

void CStudAddDlg::OnBnClickedStudent()
{
	CWnd* pCtrl = GetDlgItem(IDC_EDIT_ID);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl = GetDlgItem(IDC_STATIC__ID);
	pCtrl->ShowWindow(SW_HIDE);
}


void CStudAddDlg::OnBnClickedCaptain()
{
	CWnd* pCtrl = GetDlgItem(IDC_EDIT_ID);
	pCtrl->ShowWindow(SW_SHOW);
	pCtrl = GetDlgItem(IDC_STATIC__ID);
	pCtrl->ShowWindow(SW_SHOW);
}



void CStudAddDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}


void CStudAddDlg::OnBnSetfocusStudent()
{
	
	// TODO: Add your control notification handler code here
}


void CStudAddDlg::OnBnSetfocusCaptain()
{
	
	// TODO: Add your control notification handler code here
}
