
// Lab4View.cpp : implementation of the CLab4View class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCLab4.h"
#endif

#include "Lab4Doc.h"
#include "Lab4View.h"
#include "StudAddDlg.h"
#include "ListStudDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLab4View

IMPLEMENT_DYNCREATE(CLab4View, CScrollView)

BEGIN_MESSAGE_MAP(CLab4View, CScrollView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CScrollView::OnFilePrintPreview)
	ON_COMMAND(ID_MODIFY_ADD, OnAdd)
	ON_COMMAND(ID_MODIFY_PRINT, &CLab4View::OnModifyPrint)
	ON_COMMAND(ID_MODIFY_CLEAR, &CLab4View::OnModifyClear)
	ON_COMMAND(ID_MODIFY_LIST, &CLab4View::OnModifyList)
END_MESSAGE_MAP()

// CLab4View construction/destruction

CLab4View::CLab4View()
{
	// TODO: add construction code here

}

CLab4View::~CLab4View()
{
}

BOOL CLab4View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}



// CLab4View printing

BOOL CLab4View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CLab4View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CLab4View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CLab4View diagnostics

#ifdef _DEBUG
void CLab4View::AssertValid() const
{
	CScrollView::AssertValid();
}

void CLab4View::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CLab4Doc* CLab4View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLab4Doc)));
	return (CLab4Doc*)m_pDocument;
}
#endif //_DEBUG


// CLab4View drawing

void CLab4View::OnDraw(CDC* pDC)
{
	CLab4Doc* pDoc = GetDocument();	
	OnPaint(pDC);
}


// CLab4View message handlers

void CLab4View::OnPaint(CDC* pDC)
{
	PrintGroup(pDC);
}


// CMyScrollView drawing

void CLab4View::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
}

void CLab4View::OnAdd()
{
	CStudAddDlg dlg;
	dlg.m_eRdoGrp = 0;
	Group &students = GetDocument()->students;
	if (IDOK == dlg.DoModal())
	{
		
		if (!dlg.m_eRdoGrp)
		{
			students.AddStudentTeleshinina(
				dlg.m_sSName, dlg.m_sName, dlg.m_sSurName,
				dlg.m_iAge, dlg.m_dPoint);
		}

		if (dlg.m_eRdoGrp)
		{
			
			students.AddCaptainTeleshinina(
				dlg.m_sSName, dlg.m_sName, dlg.m_sSurName,
				dlg.m_iAge, dlg.m_dPoint, dlg.m_iId);
		}

		GetDocument()->SetModifiedFlag(TRUE);
	}
	
	
	Invalidate();
	UpdateWindow();
}

void CLab4View::PrintGroup(CDC* pDC)
{
	Group &students = GetDocument()->students;
	CString csText = students.Print();
	//pDC->TextOutA(10, 10, csText);
	CRect rcText(0, 0, 0, 0);
	
	pDC->DrawText(csText, &rcText, DT_CALCRECT);
	pDC->DrawText(csText, &rcText, DT_LEFT);
	CSize size = students.GetTextSize();
	SetScrollSizes(MM_TEXT, size);
	
}

void CLab4View::OnModifyPrint()
{
	Invalidate();
	UpdateWindow();
}

void CLab4View::OnModifyClear()
{
	Group &students = GetDocument()->students;
	students.Clear();
	
	Invalidate();
	UpdateWindow();
	
	
}
/*����� ����, ��� ���-�� �� ���:
	Invalidate();
	UpdateWindow();
�������� OnDraw();
� �� � ���� ������� ��� ����� OnPaint()
������� �������� ��� ������� ��������� PrintGroup()
������� ������� ����� � ��������� ������
*/

void CLab4View::OnModifyList()
{
	CListStudDlg dlg;
	dlg.group = &(GetDocument()->students);
	

	if (IDOK == dlg.DoModal())
	{
		
		
		GetDocument()->SetModifiedFlag(TRUE);
	}

	


	Invalidate();
	UpdateWindow();
}

