#include <iostream>
#include <fstream>
#include <cstring>
#include <md5.hpp>
#include <time.h>
#include <thread>
#include <future>
#include <vector>
#include <functional>



int main(int argc, char *argv[])
{

  std::cout << "parallel threads: " << std::thread::hardware_concurrency() << std::endl;
  clock_t begin = clock();
  //std::ifstream in("D:\\pgm\\work\\VeeamTestWork\\Mbtest", std::ifstream::binary);
  //run time 310 without threads
  //run time 295 with two threads
  //run time 285 with four threads
  //std::ifstream in("D:\\pgm\\work\\VeeamTestWork\\tmp_", std::ifstream::binary);
  std::ifstream in("D:\\virt_box\\Kali-Linux-2.0.0-vbox-amd64\\Kali-Linux-2.0.0-vbox-amd64-disk1.vmdk", std::ifstream::binary);
  //run time: 179244 without threads (char*) (1Mb block)
  //run time: 178338 without threads (only string) (1Mb block)
  //run time: 183277 with four threads (only string) (1Mb block)
  //run time: 216260 with four threads (only string) (256Mb block)
  std::ofstream out("D:\\pgm\\work\\VeeamTestWork\\output.txt");

  if (out.fail() || in.fail())
    {
        std::cout << "Input or Output file not found!" << std::endl;
        system("pause");
        return 0;
    }




  
  
  
  
  in.seekg(0, std::ios_base::end);
  long long length = in.tellg();
  in.seekg(0, std::ios_base::beg);

  unsigned int length_of_block = 1024*1024*64;
  char *buffer = new char [length_of_block];
  //std::string *strbuffer = new std::string(length_of_block);
  //long long counter = 0;

  while(!in.eof())
    {
      //One thread
      /*in.read(buffer, length_of_block);
      if(in.eof()) break;
      out << md5(std::string(buffer, length_of_block));*/

      //Multi thread
      /*in.read(buffer, length_of_block);
      if(in.eof()) break;
      std::future<std::string> th1 = std::async(md5, std::string(buffer, length_of_block));

      in.read(buffer, length_of_block);
      if(in.eof()) {break; out << th1.get(); }
      std::future<std::string> th2 = std::async(md5, std::string(buffer, length_of_block));

      in.read(buffer, length_of_block);
      if(in.eof()) {break; out << th1.get() << th2.get();}
      std::future<std::string> th3 = std::async(md5, std::string(buffer, length_of_block));

      in.read(buffer, length_of_block);
      if(in.eof()) {break; out << th1.get() << th2.get() << th3.get() ;}
      std::future<std::string> th4 = std::async(md5, std::string(buffer, length_of_block));

      out << th1.get();
      out << th2.get();
      out << th3.get();
      out << th4.get();*/

      //Without future
      in.read(buffer, length_of_block);
      if(in.eof()) break;
      std::thread th1(md5, std::string(buffer, length_of_block));

      in.read(buffer, length_of_block);
      if(in.eof()) break;
      std::thread th2(md5, std::string(buffer, length_of_block));

      in.read(buffer, length_of_block);
      if(in.eof()) break;
      std::thread th3(md5, std::string(buffer, length_of_block));

      in.read(buffer, length_of_block);
      if(in.eof()) break;
      std::thread th4(md5, std::string(buffer, length_of_block));

       th1.join();
       th2.join();
       th3.join();
       th4.join();
    }

  if(length%length_of_block != 0)
    {
      memset(buffer + length%length_of_block, 0, length_of_block - length%length_of_block);
      //std::cout <<  std::string(buffer, length_of_block) << std::endl;
      out << md5(std::string(buffer, length_of_block));

    }

  std::cout << std::endl << "size = " << length << " b" << std::endl ;
  std::cout << "size = " << (double)length/1024 << " Kb"  <<std::endl;
  std::cout << "size = " << (double)length/(1024*1024) << " Mb"  <<std::endl;

  //std::cout << "md5 of \"grape\": " << md5("grape", 5)<< std::endl;



  out.close();
  in.close();
  clock_t end = clock();
  std::cout <<"run time: " << (end - begin) << std::endl;
  return 0;
}
