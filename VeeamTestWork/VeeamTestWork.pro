#-------------------------------------------------
#
# Project created by QtCreator 2015-09-28T17:08:19
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = VeeamTestWork
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += staticlib c++11
CONFIG   += lpthread

TEMPLATE = app


SOURCES += main.cpp \
    md5.cpp

HEADERS += \
    md5.hpp


