#pragma once
#include "ModelDimitrieva.h"
class FotomodelDimitrieva : public ModelDimitrieva
{
private:
	CString NameMagazine_;
	CString Code_;

public:
	DECLARE_SERIAL(FotomodelDimitrieva)
	void readData();
	void writeData();
	void Serialize(CArchive &ar);

	FotomodelDimitrieva();
	~FotomodelDimitrieva();

};

