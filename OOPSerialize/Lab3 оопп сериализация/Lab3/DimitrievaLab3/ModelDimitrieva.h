#pragma once
#include <SDKDDKVer.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <afx.h>

class ModelDimitrieva :
	public CObject
{
public:
	DECLARE_SERIAL(ModelDimitrieva)

	virtual void readData();
	virtual void writeData();
	//bool readData_file(ifstream& f);
	//void writeData_file(ofstream& f);
	void Serialize(CArchive& ar);
	ModelDimitrieva();
	~ModelDimitrieva();
private:
	CString Surname_;
	CString Name_;
	CString Patrname_;
	CString Age_;
	CString Growth_;
	CString Weight_;
};

