#pragma once
#include "ModelDimitrieva.h"
#include "FotomodelDimitrieva.h"
#include <vector>
#include <iterator>
#include <memory>
class ModelAgencyDimitrieva
{
private:
	vector <shared_ptr<ModelDimitrieva>> base;
public:
	ModelAgencyDimitrieva();
	~ModelAgencyDimitrieva();
	void insertModel();
	void insertFotomodel();
	void showModelAgency();

	bool read_file(string);
	bool write_file(string);
	void clear();


};

