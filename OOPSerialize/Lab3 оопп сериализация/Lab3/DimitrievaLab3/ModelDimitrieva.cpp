#include "stdafx.h"
#include "ModelDimitrieva.h"
#include <sstream>


IMPLEMENT_SERIAL(ModelDimitrieva, CObject, 1)


ModelDimitrieva::ModelDimitrieva()
{

}

ModelDimitrieva::~ModelDimitrieva()
{
}

void ModelDimitrieva::readData(){

	string Surname;
	string Name;
	string Patrname;
	string Age;
	string Growth;
	string Weight;

	cout << "Surname" << endl;
	cin >> Surname;
	Surname_ = Surname.c_str();

	cout << "Name" << endl;
	cin >> Name;
	Name_ = Name.c_str();
	cout << "Patrname" << endl;
	cin >> Patrname;
	Patrname_ = Patrname.c_str();
	cout << "Age" << endl;
	cin >> Age;
	Age_ = Age.c_str();
	cout << "Growth" << endl;
	cin >> Growth;
	Growth_ = Growth.c_str();
	cout << "Weight" << endl;
	cin >> Weight;
	Weight_ = Weight.c_str();
}


void ModelDimitrieva::writeData(){
	cout << "Surname:" << Surname_ << endl;;
	cout << "Name:" << Name_ << endl;;
	cout << "Patrname:" << Patrname_ << endl;;
	cout << "Age:" << Age_ << endl;;
	cout << "Growth:" << Growth_ << endl;;
	cout << "Weight:" << Weight_ << endl;;
}



void ModelDimitrieva::Serialize(CArchive &ar)
{
	//CObject::Serialize(ar);
	if (ar.IsStoring())
	{

		ar << Surname_ << Name_ << Patrname_ << Age_ << Growth_ << Weight_;
	}
	if (ar.IsLoading())
	{

		ar >> Surname_ >> Name_ >> Patrname_ >> Age_ >> Growth_ >> Weight_;

	}
}