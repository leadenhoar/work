// lab.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ModelAgencyDimitrieva.h"


using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	ModelAgencyDimitrieva agency;
	int count = 0;
 	bool var = true;

	while (1){
		cout << "Menu: " << endl;
		cout << "1-Create new model " << endl;
		cout << "2-Create  Fotomodel " << endl;
		cout << "3-Show " << endl;
		cout << "4-Read from file " << endl;
		cout << "5-Write to file " << endl;
		cout << "6-Clear " << endl;
		cout << "7-Exit" << endl;

		cin >> count;


		switch (count) {
		case 1:
			agency.insertModel();

			break;
		case 2:
			agency.insertFotomodel();

			break;
		case 3:

			agency.showModelAgency();
			break;
		case 4:
		{
			cout << "Enter the path to the file:" << endl;
			string filename;
			cin >> filename;
			if (agency.read_file(filename)){
				cout << " Success!" << endl;
				cout << "For return to menu ";  system("pause");
				system("cls");
			}
			else{
				cout << "Please, check your file!" << endl;
				cout << "For return to menu ";  system("pause");
				system("cls");
			}
			break;
		}
		case 5:
		{
			cout << "Enter the path to the file:" << endl;
			string filename;
			cin >> filename;
			if (agency.write_file(filename) == 1){
				cout << "Success!" << endl;
				cout << "For return to menu ";  system("pause");
				system("cls");
			}
			else{
				cout << "Base is empty!" << endl;
				cout << "For return to menu ";  system("pause");
				system("cls");
			}

			break;
		}
		case 6:
		{agency.clear();
		cout << "Success!" << endl;
		cout << "For return to menu ";  system("pause");
		system("cls");

		break;
		}
		case 7:
			return 0;
		default:
			cout << "ERROR!" << endl;
			break;

		}
	}

	system("pause");
	return 0;
}

