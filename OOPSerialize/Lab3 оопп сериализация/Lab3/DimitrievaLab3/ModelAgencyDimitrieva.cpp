#include "stdafx.h"
#include "ModelAgencyDimitrieva.h"

ModelAgencyDimitrieva::ModelAgencyDimitrieva()
{
}


ModelAgencyDimitrieva::~ModelAgencyDimitrieva()
{
	//clear();
}


void ModelAgencyDimitrieva::insertModel(){
	system("cls");
	shared_ptr<ModelDimitrieva> model = make_shared<ModelDimitrieva>();
	model->readData();
	base.push_back(model);
	cout << "Added!" << endl;
	system("pause");
	system("cls");

}

void ModelAgencyDimitrieva::insertFotomodel(){
	system("cls");
	shared_ptr<ModelDimitrieva> model = make_shared<FotomodelDimitrieva>();
	model->readData();
	base.push_back(model);
	cout << "Added!" << endl;
	system("pause");
	system("cls");
}


void ModelAgencyDimitrieva::showModelAgency() {
	system("cls");
	for (auto i = base.begin(); i != base.end(); ++i){
		(*i)->writeData();
		cout << endl;
	}
	system("pause");
	system("cls");
}



bool ModelAgencyDimitrieva::read_file(string filename){
	

	base.clear();
	CFile in;
	if (!in.Open(filename.c_str(), CFile::modeRead))
		return 0;
	CArchive ar(&in, CArchive::load);

	unsigned int size;
	ar >> size; //deserialize container size;


	ModelDimitrieva* model;


	for (auto i = 0; i < size; ++i)
	{
		ar >> model;
		base.push_back(shared_ptr<ModelDimitrieva>(model));
	}


	ar.Close();
	in.Close();

	return 1;




}



bool ModelAgencyDimitrieva::write_file(string filename){
	CFile out;
	if (!out.Open(filename.c_str(), CFile::modeWrite | CFile::modeCreate))
		return 0;

	CArchive ar(&out, CArchive::store);
	ar << base.size();  //serializing elements number
	for (auto i = base.begin(); i != base.end(); ++i)
	{
		ar << i->get(); //serialize raw pointers from shared ptrs
	}


	ar.Close();
	out.Close();
	return 1;
}


void ModelAgencyDimitrieva::clear() {

	base.clear();
}

