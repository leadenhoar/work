#pragma once

#include <afxstr.h>
#include <atlimage.h>
#include <comdef.h>


// CMyScrollView view

class CMyScrollView : public CScrollView
{
	DECLARE_DYNCREATE(CMyScrollView)

protected:
	CMyScrollView();           // protected constructor used by dynamic creation
	virtual ~CMyScrollView();

private:
	CImage imgOriginal;
	int	m_nFilterLoad;
	SizesEnum m_nImageSize;

protected:
	void OnPaint(CDC* pDC);
public:
	void OnFileOpenimage(void);
	void OnFileSaveImage(void);
	void OnToolsMakeBW(void);
	void OnImageInfo(void);
	void OnUpdateSizeHalf(CCmdUI* pCmdUI);
	void OnUpdateSizeOriginal(CCmdUI* pCmdUI);
	void OnUpdateSizeDouble(CCmdUI* pCmdUI);
	void OnUpdateSizeFill(CCmdUI* pCmdUI);
	void OnChangeSize(UINT nID);
	void OnUpdateImage(CCmdUI* pCmdUI);

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct

	DECLARE_MESSAGE_MAP()
};


