#include <GL/freeglut.h>
#include <vector>
#include <math.h>
#define PI 3.14159265358979323846
static int year = 0, day = 0;

int p = 3;

float genColor()
{
	return (rand() % 100 + 0.1) / 100;
}

void init(void)
{
	glClearColor(0, 0, 0, 0);
	glShadeModel(GL_FLAT);

	
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0f, 0.5f, 0.0f);
	float base_wire_color = 0.8;
	double x0 = 0, y0 = -1, z0_1 = 0.5, z0_2 = z0_1 + 0.8f; // �����
	double alpha = 4;        // �������
	int n = 3;               // ����� ������
	double R_base = 5, r_base = 5;   // �������


	//������� ������ ������ ��� �������
	if (p == 1)
	{
		glBegin(GL_POLYGON);
		float massx[10];
		float massy[10];
		double a = alpha, da = PI / n, l;
		int ind = 0;
		for (int k = 0; k < 2 * n + 1; k++)
		{
			l = k % 2 == 0 ? r_base : R_base;
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
			massx[ind] = (float)(x0 + l * cos(a));
			massy[ind] = (float)(y0 + l * sin(a));
			a += da;
		}

		glEnd();

		glBegin(GL_POLYGON);
		a = alpha,
			da = PI / n, l;
		for (int k = 0; k < 2 * n + 1; k++)
		{
			l = k % 2 == 0 ? r_base : R_base;
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
			a += da;
		}
		glEnd();

		glBegin(GL_POLYGON);
		a = alpha,
			da = PI / n, l;
		for (int k = 0; k < 2 * n + 1; k++)
		{
			l = k % 2 == 0 ? r_base : R_base;
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
			a += da;
		}
		glEnd();
	}


	//������ ������ 
	if (p == 2)
	{
		
		glColor3f(base_wire_color, base_wire_color, base_wire_color);
		glBegin(GL_LINE_LOOP);
			
		float massx[10];
		float massy[10];
		double a = alpha, da = PI / n, l;
		int ind = 0;
		for (int k = 0; k < 2 * n + 1; k++)
		{
			l = k % 2 == 0 ? r_base : R_base;
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
			massx[ind] = (float)(x0 + l * cos(a));
			massy[ind] = (float)(y0 + l * sin(a));
			a += da;
		}

		glEnd();

		
		glColor3f(base_wire_color, base_wire_color, base_wire_color);

		glBegin(GL_LINE_LOOP);
		a = alpha,
			da = PI / n, l;
		for (int k = 0; k < 2 * n + 1; k++)
		{
			l = k % 2 == 0 ? r_base : R_base;
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
			a += da;
		}
		glEnd();

		glBegin(GL_LINES);
		a = alpha,
			da = PI / n, l;
		for (int k = 0; k < 2 * n + 1; k++)
		{
			l = k % 2 == 0 ? r_base : R_base;
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
			glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
			a += da;
		}

		glEnd();
		
	}

	
		//� ������� � ������
		if (p == 3)
		{
			glBegin(GL_POLYGON);
			
			double a = alpha, da = PI / n, l;
			int ind = 0;
			for (int k = 0; k < 2 * n + 1; k++)
			{
				l = k % 2 == 0 ? r_base : R_base;
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
				a += da;
			}

			glEnd();

			glBegin(GL_POLYGON);
			a = alpha,
				da = PI / n, l;
			for (int k = 0; k < 2 * n + 1; k++)
			{
				l = k % 2 == 0 ? r_base : R_base;
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
				a += da;
			}
			glEnd();

			glBegin(GL_POLYGON);
			a = alpha,
				da = PI / n, l;
			for (int k = 0; k < 2 * n + 1; k++)
			{
				l = k % 2 == 0 ? r_base : R_base;
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
				a += da;
			}

			glEnd();

			
			glColor3f(base_wire_color, base_wire_color, base_wire_color);

			glBegin(GL_LINE_LOOP);
			a = alpha, da = PI / n, l;
			for (int k = 0; k < 2 * n + 1; k++)
			{
				l = k % 2 == 0 ? r_base : R_base;
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
				a += da;
			}

			glEnd();

			glBegin(GL_LINE_LOOP);
			a = alpha,
				da = PI / n, l;
			for (int k = 0; k < 2 * n + 1; k++)
			{
				l = k % 2 == 0 ? r_base : R_base;
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
				a += da;
			}
			glEnd();

			glBegin(GL_LINES);
			a = alpha,
				da = PI / n, l;
			for (int k = 0; k < 2 * n + 1; k++)
			{
				l = k % 2 == 0 ? r_base : R_base;
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_1);
				glVertex3f((float)(x0 + l * cos(a)), (float)(y0 + l * sin(a)), z0_2);
				a += da;
			}

			glEnd();

		}
	
		
	//������ ������
	for (int i = 0; i < 1; i++)
	{
		//������ �����
		float sign = rand() % 2 - 1;
		float c = (rand() % 5) - 0.5;
		float s = (rand() % 5) - 0.5;
		float t = (rand() % 3) + 1;
		float r = (rand() % 7) + 3;
		r = r / 10;
		glPushMatrix();
		glTranslatef(c, s, t);
		glColor3f(genColor(), genColor(), genColor());
		
		if (p == 1)
		{
			glutSolidSphere(r, 20, 20);
		}
		if (p == 2)
		{
			glutWireSphere(r, 20, 20);
		}
		if (p == 3)
		{
			glutSolidSphere(r, 15, 15);
			glColor3f(base_wire_color, base_wire_color, base_wire_color);
			glutWireSphere(r, 20, 20);
		}
		glPopMatrix();



		//������ ���
		
		
		
		glColor3f(genColor(), genColor(), genColor());
		c = (rand() % 5) - 0.5;
		s = (rand() % 5) - 0.5;
		glPushMatrix();
		glTranslatef(c, s, t);
		r = (rand() % 7) + 3;
		r = r / 10;
		if (p == 1)
		{
			glutSolidCube(r);
		}
		if (p == 2)
		{
			glutWireCube(r);
		}
		if (p == 3)
		{
			glRotatef((GLfloat)0.5, 0.0, 1.0, 0.0);
			glutSolidCube(r);
			
			
			
			glColor3f(genColor(), genColor(), genColor());
			glutWireCube(r);
		}
		glPopMatrix();

 

		//������ �������
		
		
		
		glColor3f(genColor(), genColor(), genColor());
		c = (rand() % 5) - 0.5;
		s = (rand() % 5) - 0.5;
		glPushMatrix();
		glTranslatef(c*sign, s*sign, t);

		if (p == 1)
		{
			glutSolidOctahedron();
		}
		if (p == 2)
		{
			glutWireOctahedron();
		}
		if (p == 3)
		{
			glutSolidOctahedron();
			glColor3f(base_wire_color, base_wire_color, base_wire_color);
			glutWireOctahedron();
		}
		glPopMatrix();


		//������ ��������
		
		
		
		glColor3f(genColor(), genColor(), genColor());
		c = (rand() % 5) - 0.5;
		s = (rand() % 5) - 0.5;
		glPushMatrix();
		glTranslatef(c, s, t);
		if (p == 1)
		{
			glutSolidTetrahedron();
		}
		if (p == 2)
		{
			glutWireTetrahedron();
		}
		if (p == 3)
		{
			glutSolidTetrahedron();
			glColor3f(base_wire_color, base_wire_color, base_wire_color);
			glutWireTetrahedron();			
		}
		glPopMatrix();


		
		//������ ���������
		
		
		
		glColor3f(genColor(), genColor(), genColor());
		c = (rand() % 5) - 5 ;
		s = (rand() % 5) - 5 ;
		glPushMatrix();
		glTranslatef(c, s, t);
		
		if (p == 1)
		{
			glutSolidDodecahedron();
		}
		if (p == 2)
		{
			glutWireDodecahedron();
		}
		if (p == 3)
		{
			glutSolidDodecahedron();
			glColor3f(base_wire_color, base_wire_color, base_wire_color);
			glutWireDodecahedron();
		}		
		glPopMatrix();

	}
	glutSwapBuffers();
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h); // ����� ������� ������� ������ � ������������� ��������������

	glMatrixMode(GL_PROJECTION);  // �������� ������� �������� 
	glLoadIdentity(); // ����� ������� ��������

	//glFrustum(-1.0, 1.0, -1.0, 1.0, 1.5, 20.0);
	gluPerspective(60.0, (GLfloat)w / (GLfloat)h, 1.0, 20.0);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	gluLookAt(  0.0, 9.0, 5.0, 
				0.0, 0.0, 0.0, 
				0.0, -1.0, 0.0);

}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case '1':
		p = 1;
		glutPostRedisplay();
		break;
	case '2':
		p = 2;
		glutPostRedisplay();
		break;
	case '3':
		p = 3;
		glutPostRedisplay();
		break;
	default:
		break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv); //������������� glut
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); //������������ ���� ����� ��� ������ � 3 ���������� �����
	glutInitWindowSize(500, 500); //������ ����
	glutInitWindowPosition(100, 100); //��������� ������������ �������� ������ ����
	glutCreateWindow(argv[0]); // ���������� �������� HANDLER
	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape); //�������� �� ��������� ������� ����
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return 0;
}
