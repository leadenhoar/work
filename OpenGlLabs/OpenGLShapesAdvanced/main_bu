// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <iomanip>
#include <vector>
#include <iostream>
#include <time.h>
#include <chrono>

// Include GLEW
#include <GL/glew.h>


// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>
using namespace glm;
using namespace std;

#include <common/shader.hpp>
//#include <common/texture.hpp>
#include <common/controls.hpp>
#include <common/objloader.hpp>
#include <common/quaternion_utils.hpp>

#define PI 3.14159265358979323846
static int year = 0, day = 0;

vec3 gPosition1(-1.5f, 0.0f, 0.0f);
vec3 gOrientation1;

vec3 gPosition2(1.5f, 0.0f, 0.0f);
quat gOrientation2;

bool gLookAtOther = true;

float genColor()
{
	return (rand() % 100 + 0.1) / 100;
}


void gen_base(vector<GLfloat> &g_vertex, vector<GLfloat> &g_uv)
{
	vector<GLfloat> g_vertex_buffer_data(3 * 7);
	vector<GLfloat> g_vertex_buffer_data_bottom(3 * 7);
	float base_wire_color = 0.8;

	double x0 = 0, y0 = 0, z0_1 = 0, z0_2 = z0_1 + 0.3f; // центр
	double alpha = 0;           // поворот
	int n = 6;                  // число вершин
	double R_base = 0.5;			// радиусы



	double a = alpha, da = 2 * (PI / n);
	for (int i = 0; i <= n; i++)
	{
		float x = (float)(x0 + R_base * cos(a));
		float y = (float)(y0 + R_base * sin(a));
		float z = z0_1;
		float norm = sqrt(x*x + y*y + z*z);
		norm = 1;
		g_vertex_buffer_data[i * 3] = x / norm;
		g_vertex_buffer_data[i * 3 + 1] = y / norm;
		g_vertex_buffer_data[i * 3 + 2] = z / norm;

		z = z0_2;
		//norm = sqrt(x*x + y*y + z*z);
		
		g_vertex_buffer_data_bottom[i * 3] = x / norm;
		g_vertex_buffer_data_bottom[i * 3 + 1] = y / norm;
		g_vertex_buffer_data_bottom[i * 3 + 2] = z0_2 / norm;
		
		a += da;
	}

	ofstream out("shapes/base.obj");
	if (out.is_open())
	{
		out << "# base.obj created by hand." << endl;
		out << "#" << endl << endl;
		out << "o base" << endl << endl;
		out << "usemtl Material" << endl << endl;
		out << "s off" << endl << endl;
		for (int i = 0; i <= n; i++)
		{
			out << "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_buffer_data[i * 3] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_buffer_data[i * 3 + 1] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_buffer_data[i * 3 + 2] << " "
				<< endl;
		}
		out << endl;

		for (int i = 0; i <= n; i++)
		{
			out << "f" << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< endl;
		}
		out.close();
	}


	vector<GLfloat>  g_vertex_sh_buffer_data(3 * 3 * 4 * 6);
	vector<GLfloat> g_uv_buffer_dat;
	g_uv_buffer_dat.reserve(3 * 3 * 4 * 6);
	
	size_t seg = 3 * 3 * 4;
	for (size_t i = 0; i < n; ++i)
	{
		// Top
		g_vertex_sh_buffer_data[seg * i] = x0;
		g_vertex_sh_buffer_data[seg * i + 1] = y0;
		g_vertex_sh_buffer_data[seg * i + 2] = z0_1;

		g_vertex_sh_buffer_data[seg * i + 3] = g_vertex_buffer_data[(i)* 3];
		g_vertex_sh_buffer_data[seg * i + 4] = g_vertex_buffer_data[(i)* 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 5] = g_vertex_buffer_data[(i)* 3 + 2];

		g_vertex_sh_buffer_data[seg * i + 6] = g_vertex_buffer_data[(i + 1) * 3];
		g_vertex_sh_buffer_data[seg * i + 7] = g_vertex_buffer_data[(i + 1) * 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 8] = g_vertex_buffer_data[(i + 1) * 3 + 2];

		//Bottom
		g_vertex_sh_buffer_data[seg * i + 9] = x0;
		g_vertex_sh_buffer_data[seg * i + 10] = y0;
		g_vertex_sh_buffer_data[seg * i + 11] = z0_2;

		g_vertex_sh_buffer_data[seg * i + 12] = g_vertex_buffer_data_bottom[(i)* 3];
		g_vertex_sh_buffer_data[seg * i + 13] = g_vertex_buffer_data_bottom[(i)* 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 14] = g_vertex_buffer_data_bottom[(i)* 3 + 2];

		g_vertex_sh_buffer_data[seg * i + 15] = g_vertex_buffer_data_bottom[(i + 1) * 3];
		g_vertex_sh_buffer_data[seg * i + 16] = g_vertex_buffer_data_bottom[(i + 1) * 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 17] = g_vertex_buffer_data_bottom[(i + 1) * 3 + 2];

		//Side
		g_vertex_sh_buffer_data[seg * i + 18] = g_vertex_buffer_data[(i)* 3];
		g_vertex_sh_buffer_data[seg * i + 19] = g_vertex_buffer_data[(i)* 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 20] = g_vertex_buffer_data[(i)* 3 + 2];

		g_vertex_sh_buffer_data[seg * i + 21] = g_vertex_buffer_data[(i + 1) * 3];
		g_vertex_sh_buffer_data[seg * i + 22] = g_vertex_buffer_data[(i + 1) * 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 23] = g_vertex_buffer_data[(i + 1) * 3 + 2];

		g_vertex_sh_buffer_data[seg * i + 24] = g_vertex_buffer_data_bottom[(i + 1) * 3];
		g_vertex_sh_buffer_data[seg * i + 25] = g_vertex_buffer_data_bottom[(i + 1) * 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 26] = g_vertex_buffer_data_bottom[(i + 1) * 3 + 2];

		//Side
		g_vertex_sh_buffer_data[seg * i + 27] = g_vertex_buffer_data[(i)* 3];
		g_vertex_sh_buffer_data[seg * i + 28] = g_vertex_buffer_data[(i)* 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 29] = g_vertex_buffer_data[(i)* 3 + 2];

		g_vertex_sh_buffer_data[seg * i + 30] = g_vertex_buffer_data_bottom[(i)* 3];
		g_vertex_sh_buffer_data[seg * i + 31] = g_vertex_buffer_data_bottom[(i)* 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 32] = g_vertex_buffer_data_bottom[(i)* 3 + 2];

		g_vertex_sh_buffer_data[seg * i + 33] = g_vertex_buffer_data_bottom[(i + 1) * 3];
		g_vertex_sh_buffer_data[seg * i + 34] = g_vertex_buffer_data_bottom[(i + 1) * 3 + 1];
		g_vertex_sh_buffer_data[seg * i + 35] = g_vertex_buffer_data_bottom[(i + 1) * 3 + 2];

	}

	

	out.open("shapes/base_sh.obj");
	if (out.is_open())
	{
		out << "# base.obj created by hand." << endl;
		out << "#" << endl;// << endl;
		out << "o sh_base" << endl;// << endl;
		for (size_t i = 0; i < n; ++i)
		{
			out //<< "#Top" << endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 1] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 2] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 3] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 4] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 5] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 6] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 7] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 8] << " "
				<< endl
				
				//<< "#Bottom" << endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 9] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 10] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 11] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 12] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 13] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 14] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 15] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 16] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 17] << " "
				<< endl
				
				//<< "#Side" << endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 18] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 19] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 20] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 21] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 22] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 23] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 24] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 25] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 26] << " "
				<< endl
				//<< "#Side" << endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 27] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 28] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 29] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 30] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 31] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 32] << " "
				<< endl
				<< "v" << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 33] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 34] << " "
				<< std::fixed << std::setprecision(4) << g_vertex_sh_buffer_data[seg * i + 35] << " "
				<< endl;
		}
		
		//out << endl;
		out << "usemtl Material" << endl;// << endl;
		out << "s off" << endl;// << endl;
		for (int i = 0; i < n; i++)
		{
			for (size_t j = 0; j < 12; ++j)
			{
				g_uv_buffer_dat.push_back(0);
				g_uv_buffer_dat.push_back(0.8);
				g_uv_buffer_dat.push_back(0);
			}
			
			out //<< "#Top" << endl
				<< "f" << " "
				<< 0 << " "
				<< 0.8 << " "
				<< 0 << " "
				<< endl
				<< "f" << " "
				<< 0 << " "
				<< 0.8 << " "
				<< 0 << " "
				<< endl
				<< "f" << " "
				<< 0 << " "
				<< 0.8 << " "
				<< 0 << " "
				<< endl

				//<< "#Bottom" << endl
				<< "f" << " "
				<< 0 << " "
				<< 0.8 << " "
				<< 0 << " "
				<< endl
				<< "f" << " "
				<< 0 << " "
				<< 0.8 << " "
				<< 0 << " "
				<< endl
				<< "f" << " "
				<< 0 << " "
				<< 0.8 << " "
				<< 0 << " "
				<< endl

				//<< "#Side" << endl
				<< "f" << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< endl
				<< "f" << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< endl
				<< "f" << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< endl

				//<< "#Side" << endl
				<< "f" << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< endl
				<< "f" << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< endl
				<< "f" << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< 0.8 << " "
				<< endl;
					
				
		}
		out.close();
	}

	g_vertex = g_vertex_sh_buffer_data;
	g_uv = g_uv_buffer_dat;
}


int main(void)
{
	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(1024, 768, "Shapes", NULL, NULL);
	if (window == NULL){
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Инициализируем GLEW
	glewExperimental = true; // Флаг необходим в Core - режиме OpenGL
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders("TransformVertexShader.vertexshader", "ColorFragmentShader.fragmentshader");

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	
	//каркас основы 
	vector<GLfloat> vertices_;
	vector<GLfloat> uvs_;
	gen_base(vertices_, uvs_);

	
	// Read our .obj file
	///////////////////////////////////////////////////////////////////////////////////////////
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals; // Won't be used at the moment.
	//bool res = loadOBJ("box.obj", vertices, uvs, normals);
	///////////////////////////////////////////////////////////////////////////////////////////


	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices_.size() * (sizeof(GLfloat)), &vertices_[0], GL_STATIC_DRAW);


	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs_.size() * sizeof(GLfloat), &uvs_[0], GL_STATIC_DRAW);

	///////////////////////////////////////////////////////////////////////////////////////////
	glm::mat4 trans;
	trans = glm::rotate(trans, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	glm::vec4 result = trans * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	printf("%f, %f, %f\n", result.x, result.y, result.z);
	GLint uniTrans = glGetUniformLocation(programID, "trans");
	glUniformMatrix4fv(uniTrans, 1, GL_FALSE, glm::value_ptr(trans));


	auto t_start = chrono::high_resolution_clock::now();
	do{

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Calculate transformation
		auto t_now = std::chrono::high_resolution_clock::now();
		float time = std::chrono::duration_cast<std::chrono::duration<float>>(t_now - t_start).count();

		glm::mat4 trans;
		trans = glm::rotate(
			trans,
			time * glm::radians(180.0f),
			glm::vec3(0.0f, 0.0f, 1.0f)
			);
		glUniformMatrix4fv(uniTrans, 1, GL_FALSE, glm::value_ptr(trans));



		// Compute the MVP matrix from keyboard and mouse input
		computeMatricesFromInputs();
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			2,                                // size : U+V => 2
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, vertices_.size()); 
		//glDrawArrays(GL_LINE_LOOP, 0, vertices.size());

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while ( glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS 
		&&	glfwWindowShouldClose(window) == 0);

	// Cleanup VBO
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);
	
	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
