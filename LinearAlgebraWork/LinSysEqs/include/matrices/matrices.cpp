#include <iostream>
#include <iomanip>
#include <utility>
#include <algorithm>
#include <fstream>
#include <stdexcept>
#include "matrices.hpp"

Matrix::Matrix()
{
	rows_ = columns_ = 0;
	matrix_ = nullptr;
}
Matrix::Matrix(char const *path)
{
    std::ifstream input(path);
    if(input)
    {
        input >> rows_;
        input >> columns_;

        matrix_ = create_matrix(rows_, columns_);
		
		for(size_t row = 0; row < rows_; ++row)
        {
            for(size_t column = 0; column < columns_; ++column)
            {
                input >> matrix_[row][column];
            }
        }
    }
    else
    {
        throw std::runtime_error("File can not be opened.");
    }

}
Matrix::Matrix(std::string file_path)
{
	std::ifstream in(file_path);
	if (in)
	{
		in >> rows_;
		in >> columns_;
		matrix_ = create_matrix(rows_, columns_);
		for (size_t i = 0; i < rows_; ++i)
		{
			for (size_t j = 0; j < columns_; ++j)
			{
				in >> matrix_[i][j];
			}
		}
	}
	else
	{
		throw std::runtime_error("File can not be opened.");
	}
}
Matrix::Matrix(size_t const rows, size_t const columns)
    : rows_(rows)
    , columns_(columns)
    , matrix_(create_matrix(rows_, columns_)){}
Matrix::Matrix(size_t const rows, size_t const columns, double const value)
	: rows_(rows)
	, columns_(columns)
	, matrix_(create_matrix(rows_, columns_))
{
	for (size_t row = 0; row < rows_; ++row)
	{
		for (size_t column = 0; column < columns_; ++column)
		{
			matrix_[row][column] = value;
		}
	}
}
Matrix::Matrix(size_t const rows, size_t const columns, double * lapack_matrix)
: rows_(rows)
, columns_(columns)
, matrix_(create_matrix(rows_, columns_))
{
	for (size_t i = 0; i < rows; ++i)
	{
		for (size_t j = 0; j < columns; ++j)
		{
			matrix_[i][j] = lapack_matrix[i * columns + j];
		}
	}
}
Matrix::Matrix(Matrix const &matrix_for_copy)
    : rows_(matrix_for_copy.rows_)
    , columns_(matrix_for_copy.columns_)
    , matrix_(create_matrix(rows_, columns_))
{
    for(size_t row = 0; row < rows_; ++row)
    {
        for(size_t column = 0; column < columns_; ++column)
        {
            matrix_[row][column] = matrix_for_copy[row][column];
        }
    }
}

Matrix &Matrix::operator=(Matrix const &matrix)
{
	if (this != &matrix)
	{
		Matrix(matrix).swap(*this);
	}
	return *this;
}
Matrix::~Matrix()
{
	delete_matrix(rows_, columns_, matrix_);
}

void Matrix::swap(Matrix &matrix)
{
    std::swap(rows_, matrix.rows_);
    std::swap(columns_, matrix.columns_);
    std::swap(matrix_, matrix.matrix_);
}
Matrix Matrix::transpose()
{
	Matrix tmp(columns_, rows_);
	for (size_t row = 0; row < rows_; ++row)
	{
		for (size_t column = 0; column < columns_; ++column)
		{
			tmp[column][row] = matrix_[row][column];
		}
	}
	return tmp;
		
}

double		 * Matrix::operator[](size_t const i)
{
    return matrix_[i];
}
double const * Matrix::operator[](size_t const i) const
{
    return matrix_[i];
}

Matrix &Matrix::operator+= (Matrix const &right_arg)
{
	if(rows_ != right_arg.rows_ || columns_ != right_arg.columns_)
    {
        throw std::runtime_error("Wrong sizes of matrices in += operation.");
    }
    for(size_t row = 0; row < rows_; ++row)
    {
        for(size_t column = 0; column < columns_; ++column)
        {
            matrix_[row][column] += right_arg[row][column];
        }
    }
    return *this;
}
Matrix &Matrix::operator-= (Matrix const &right_arg)
{
	return *this += (- Matrix(right_arg));
}
Matrix &Matrix::operator*= (Matrix const &right_arg)
{
    if(columns_ != right_arg.rows_)
    {
        throw std::runtime_error("Wrong sizes of matrices in *= operation.");
    }

    Matrix tmp_matrix(rows_, right_arg.columns_);
    for(size_t row_for_left = 0; row_for_left < rows_; ++row_for_left)
    {
        for(size_t column_for_right = 0; column_for_right < right_arg.columns_; ++column_for_right)
        {
            for(size_t index_for_sum = 0; index_for_sum < columns_ ;++index_for_sum)
            {
                tmp_matrix[row_for_left][column_for_right] += matrix_[row_for_left][index_for_sum] * right_arg[index_for_sum][column_for_right];
            }
        }

    }
    *this = tmp_matrix;
    return *this;

}
Matrix &Matrix::operator- ()
{
	for (size_t row = 0; row < rows_; ++row)
	{
		for (size_t column = 0; column < columns_; ++column)
		{
			matrix_[row][column] *= -1;
		}
	}
	return *this;
}

Matrix Matrix::get_Lower_triangular()
{
	if (rows_ != columns_)
	{
		throw std::runtime_error("Wrong sizes of matrices in get_Lower_triangular functiion.");
	}

	Matrix tmp(*this);
	for (size_t i = 0; i < rows_; ++i)
	{
		for (size_t j = i; j < columns_; ++j)
		{
			tmp[i][j] = 0;
		}
	}
	return tmp;
}
Matrix Matrix::get_Diagonal()
{
	if (rows_ != columns_)
	{
		throw std::runtime_error("Wrong sizes of matrices in get_Lower_triangular functiion.");
	}

	Matrix tmp(rows_, columns_);
	for (size_t i = 0; i < rows_; ++i)
	{
		tmp[i][i] = matrix_[i][i];
	}
	return tmp;
}
Matrix Matrix::get_Upper_triangular()
{
	if (rows_ != columns_)
	{
		throw std::runtime_error("Wrong sizes of matrices in get_Lower_triangular functiion.");
	}


	Matrix tmp(*this);
	for (size_t i = 0; i < rows_; ++i)
	{
		for (size_t j = 0; j <= i; ++j)
		{
			tmp[i][j] = 0;
		}
	}
	return tmp;

}

size_t Matrix::get_rows_() const
{
    return rows_;
}
size_t Matrix::get_columns_() const
{
    return columns_;
}

void Matrix::print(std::string disc) const
{
	std::cout << disc << ":" << std::endl << std::endl;
	//std::cout << rows_ << std::endl;
	//std::cout << columns_ << std::endl;
	for (size_t row = 0; row < rows_; ++row)
	{
		for (size_t column = 0; column < columns_; ++column)
		{
			std::cout.setf(std::ios::fixed);
			std::cout.width(8);
			std::cout.precision(5);
			std::cout << matrix_[row][column] << ' ';
		}
		std::cout << std::endl;
	}
	std::cout << std::endl << std::endl;
}
void Matrix::print_to_file(std::string disc, std::string file_path) const
{
	std::ofstream out(file_path);
	out << disc << " = ";
	//out << rows_ << std::endl;
	//out << columns_ << std::endl;
	//out << "{{";
	out << "[";
	for (size_t row = 0; row < rows_; ++row)
	{
		for (size_t column = 0; column < columns_; ++column)
		{
			out.width(8);
			out.precision(5);
			out << matrix_[row][column] << " ";
		}
		//out << "}" << std::endl;
		out << ";";
	}
	//out << "}" << std::endl << std::endl;
	out << "]" << std::endl << std::endl;
}

Matrix operator+ (Matrix const &left_arg, Matrix const &right_arg)
{
    return Matrix(left_arg) += right_arg;
}
Matrix operator- (Matrix const &left_arg, Matrix const &right_arg)
{
	return Matrix(left_arg) -= right_arg;
}
Matrix operator* (Matrix const &left_arg, Matrix const &right_arg)
{
    return Matrix(left_arg) *= right_arg;
}
Matrix create_E(size_t const rows)
{
	Matrix tmp(rows, rows);
	for (size_t i = 0; i < rows; ++i)
	{
		tmp[i][i] = 1;
	}
	return tmp;
}

double **create_matrix(size_t const rows, size_t const columns)
{
    double **matrix = new double *[rows];
    for (size_t row = 0; row < rows; ++row)
    {
        matrix[row] = new double[columns]();
    }

    return matrix;
}
double	*convert_to_lapack_matrix(Matrix m)
{
	double *mt = new double[m.get_rows_() * m.get_columns_()];
	for (size_t i = 0; i < m.get_rows_(); ++i)
	{
		for (size_t j = 0; j < m.get_columns_(); ++j)
		{
			mt[i * m.get_columns_() + j] = m[i][j];
		}
	}
	return mt;
}
double **convert_from_lapack_matrix(size_t const rows, size_t const columns, double *ml)
{
	double **m = new double*[rows];
	for (size_t i = 0; i < rows; ++i)
	{
		m[i] = new double[columns];
		for (size_t j = 0; j < columns; ++j)
		{
			m[i][j] = ml[i * columns + j];
		}
	}
	return m;
}
void	 delete_matrix(int n, int m, double **matrix)
{
	for (size_t row = 0; row < n; ++row)
	{
		delete[] matrix[row];
	}
	delete[] matrix;
}

void print_matrix(std::string desc, lapack_int n, lapack_int m, double* a, lapack_int lda)
{
	std::cout << desc << ":" << std::endl << std::endl;
	for (lapack_int i = 0; i < n; i++)
	{
		for (lapack_int j = 0; j < m; j++)
		{
			std::cout.setf(std::ios::fixed);
			std::cout.width(8);
			std::cout.precision(5);
			std::cout << a[i*lda + j];
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}
void print_int_vector(std::string desc, lapack_int n, lapack_int* a)
{
	std::cout << desc << ":" << std::endl << std::endl;
	for (lapack_int j = 0; j < n; j++)
	{
		std::cout.setf(std::ios::fixed);
		std::cout.width(8);
		std::cout.precision(5);
		std::cout << a[j] << std::endl;
	}
	std::cout << std::endl;
	std::cout << std::endl;
}

void print_double_vector(std::string desc, lapack_int n, double* a)
{
	std::cout << desc << ":" << std::endl << std::endl;
	for (lapack_int j = 0; j < n; j++)
	{
		std::cout.setf(std::ios::fixed);
		std::cout.width(8);
		std::cout.precision(5);
		std::cout << a[j] << std::endl;
	}
	std::cout << std::endl;
	std::cout << std::endl;
}

LinearSysterm::LinearSysterm(Matrix A, Matrix b)
{
	A_ = A;
	b_ = b;
}
Matrix LinearSysterm::Jacobi(size_t max_iter, double eps)
{
	std::cout << "Jacobi method sulution:" << std::endl;
	printf("Jacobi method sulution:\n");
	//A_.print("A");
	//b_.print("b");
	/*Matrix D = A_.get_Diagonal();
	Matrix revD(D);
	for (size_t i = 0; i < D.get_rows_(); ++i)
	{
		if (!revD[i][i])
		{
			throw std::runtime_error("Zero division in Jacoby method.");
		}
		revD[i][i] = (double)1 / revD[i][i];
	}

	Matrix B = revD * (D - A_);
	B.print_to_file("B", "B1.txt");
	Matrix g = revD * b_;

	//D.print("D");
	//revD.print("revD");
	//B.print("B");
	//g.print("g");

	Matrix X(b_.get_rows_(), b_.get_columns_(), 0.);
	Matrix prevX(X);
	size_t iter = 0;
	do
	{
		//std::cout << "iter: " << iter << std::endl;
		//X.print("X");
		iter++;
		prevX = X;
		X = B * prevX + g;
	} while (!delta_max_cond(prevX, X, eps) && iter < max_iter);
	if (!delta_max_cond(prevX, X, eps))
	{
		std::cout << "The method does not converge." << std::endl;
	}
	else
	{
		std::cout << "Iterations of method: " << iter << std::endl;
	}
	return X;
	*/

	Matrix X(b_.get_rows_(), b_.get_columns_(), 0.);
	Matrix prevX(X);
	size_t iter = 0;
	do
	{
		//std::cout << "iter: " << iter << std::endl;
		//X.print("X");
		iter++;
		prevX = X;
		for (size_t i = 0; i < b_.get_rows_(); ++i)
		{
			double sum = 0;
			for (size_t j = 0; j < i; ++j)
			{
				sum += X[j][0] * -A_[i][j];
			}
			for (size_t j = i + 1; j < A_.get_columns_(); ++j)
			{
				sum += X[j][0] * -A_[i][j];
			}

			X[i][0] = (sum + b_[i][0]) / A_[i][i];
		}
	} while (!delta_max_cond(prevX, X, eps) && iter < max_iter);

	if (!delta_max_cond(prevX, X, eps))
	{
		std::cout << "The method does not converge." << std::endl;
		printf("The method does not converge.\n");
	}
	else
	{
		std::cout << "Iterations of method: " << iter << std::endl;
		printf("Iterations of method:\n");
		printf("%d\n", iter);
	}
	return X;
}
Matrix LinearSysterm::Zeidel(size_t max_iter, double eps)
{
	std::cout << "Zeidel method sulution:" << std::endl;
	printf("Zeidel method sulution:\n");
	Matrix X(b_.get_rows_(), b_.get_columns_(), 0.);
	Matrix prevX(X);
	size_t iter = 0;
	do
	{
		//std::cout << "iter: " << iter << std::endl;
		//X.print("X");
		iter++;
		prevX = X;
		for (size_t i = 0; i < b_.get_rows_(); ++i)
		{
			double sum = 0;
			for (size_t j = 0; j < i; ++j)
			{
				sum += X[j][0] * -A_[i][j];
			}
			for (size_t j = i + 1; j < A_.get_columns_(); ++j)
			{
				sum += prevX[j][0] * -A_[i][j] ;
			}

			X[i][0] = (sum + b_[i][0]) / A_[i][i];			
		}
	} while (!delta_norm_cond(prevX, X, eps) && iter < max_iter);

	if (!delta_norm_cond(prevX, X, eps))
	{
		std::cout << "The method does not converge." << std::endl;
		printf("The method does not converge.\n");
	}
	else
	{
		std::cout << "Iterations of method: " << iter << std::endl;
		printf("Iterations of method:\n");
		printf("%d\n", iter);
	}
	return X;
}
Matrix LinearSysterm::Relax(double mu, size_t max_iter, double eps)
{
	std::cout << "Successive over-relaxation method sulution:" << std::endl;
	printf("Successive over-relaxation method sulution:\n");
	printf("mu = %5.2f\n", mu);
	Matrix X(b_.get_rows_(), b_.get_columns_(), 0.);
	Matrix prevX(X);
	size_t iter = 0;
	do
	{
		//std::cout << "iter: " << iter << std::endl;
		//X.print("X");
		iter++;
		prevX = X;
		for (size_t i = 0; i < b_.get_rows_(); ++i)
		{
			double sum = 0;
			for (size_t j = 0; j < i; ++j)
			{
				sum += X[j][0] * -A_[i][j];
			}
			for (size_t j = i + 1; j < A_.get_columns_(); ++j)
			{
				sum += prevX[j][0] * (-A_[i][j]);
			}

			X[i][0] = (1 - mu) * prevX[i][0] + mu * (sum + b_[i][0]) / A_[i][i];

		}

	} while (!delta_norm_cond(prevX, X, eps) && iter < max_iter);
	
	if (!delta_norm_cond(prevX, X, eps))
	{
		std::cout << "The method does not converge." << std::endl;
		printf("The method does not converge.\n");
	}
	else
	{
		std::cout << "Iterations of method: " << iter << std::endl;
		printf("Iterations of method:\n");
		printf("%d\n", iter);
	}
	return X;
}


bool delta_max_cond(Matrix const prevX, Matrix const X, double const eps)
{
	if (prevX.get_columns_() != 1 || X.get_columns_() != 1)
	{
		throw std::runtime_error("Columns in delta of X != 1");
	}
	
	for (size_t i = 0; i < X.get_rows_(); ++i)
	{
		if (std::abs(prevX[i][0] - X[i][0]) >= eps)
		{
			return false;
		}
		return true;
	}
}
bool delta_norm_cond(Matrix const prevX, Matrix const X, double const eps)
{
	if (prevX.get_columns_() != 1 || X.get_columns_() != 1)
	{
		throw std::runtime_error("Columns in delta of X != 1");
	}

	double norm = 0;
	for (size_t i = 0; i < X.get_rows_(); ++i)
	{
		norm += (prevX[i][0] - X[i][0])*(prevX[i][0] - X[i][0]);	
	}

	if (std::sqrt(norm) >= eps)
	{
		return false;
	}
	return true;
}