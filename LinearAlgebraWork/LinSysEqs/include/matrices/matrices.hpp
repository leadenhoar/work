#ifndef MATRICES_HPP
#define MATRICES_HPP

#include <cstddef>
#include "lapacke.h"
#include <string>
#include <iostream>

struct opiped {
	opiped(std::streambuf * buf, std::ostream & os)
	:os(os), old_buf(os.rdbuf(buf)) { }
	~opiped() { os.rdbuf(old_buf); }

	std::ostream& os;
	std::streambuf * old_buf;
};

class Matrix
{
public:
    Matrix();
	explicit Matrix(char const *path);
	explicit Matrix(std::string file_path);
    Matrix(size_t const rows, size_t const columns);
    Matrix(size_t const rows, size_t const columns, double const value);
	Matrix(size_t const rows, size_t const columns, double *lapack_matrix);
	Matrix(Matrix const &matrix);
    
    Matrix &operator=(Matrix const &matrix);
    ~Matrix();
    
    void swap(Matrix &matrix);
	Matrix transpose();

	double       * operator[] (size_t const i);
    double const * operator[] (size_t const i) const;
    
	Matrix &operator+= (Matrix const &right_arg);
    Matrix &operator-= (Matrix const &right_arg);
    Matrix &operator*= (Matrix const &right_arg);
	Matrix &operator- ();

	Matrix get_Lower_triangular();
	Matrix get_Diagonal();
	Matrix get_Upper_triangular();
    
    size_t get_rows_()    const;
    size_t get_columns_() const;

	void print(std::string disc) const;
	void print_to_file(std::string disc, std::string file_path) const;

private:
    size_t rows_;
    size_t columns_;
    double **matrix_;
};

Matrix operator+ (Matrix const &left_arg, Matrix const &right_arg);
Matrix operator- (Matrix const &left_arg, Matrix const &right_arg);
Matrix operator* (Matrix const &left_arg, Matrix const &right_arg);
Matrix create_E(size_t const rows);

double **create_matrix(size_t const rows, size_t const columns);
double  *convert_to_lapack_matrix(Matrix);
double **convert_from_lapack_matrix(size_t const rows, size_t const columns, double *ml);
void	 delete_matrix(int n, int m, double **matrix);
void print_matrix(std::string desc, lapack_int m, lapack_int n, double* a, lapack_int lda);
void print_int_vector(std::string desc, lapack_int n, lapack_int* a);
void print_double_vector(std::string desc, lapack_int n, double* a);

class LinearSysterm
{
public:
	LinearSysterm(Matrix A, Matrix b);
	Matrix Jacobi(size_t max_iter, double eps);
	Matrix Zeidel(size_t max_iter, double eps);
	Matrix Relax(double mu, size_t max_iter, double eps);


private:
	Matrix A_, b_;
};

bool delta_max_cond(Matrix const prevX, Matrix const X, double const eps);
bool delta_norm_cond(Matrix const prevX, Matrix const X, double const eps);
#endif // MATRICES_HPP

