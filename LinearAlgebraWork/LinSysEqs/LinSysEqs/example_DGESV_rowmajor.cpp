#include <iostream>
#include <fstream>
#include "lapacke.h"
#include "matrices.hpp"
#include <math.h>
#include <time.h>

/* Parameters */
#define N 5
#define N1 48
#define N2 510
#define N3 1280

using namespace std;

const double eps = 0.1; ///< �������� �������� 
const size_t max_iter = 15000;

/* Auxiliary routines prototypes */
void solution();
void matrix_test();
void linear_system_test();
void linear_system_solution(size_t const n);

/* Main program */
int main() 
{
	try
	{
		size_t n = 0;
		cout << "N:" << endl;
		cin >> n;
		string file_path = to_string(n) + ".txt";

		ofstream fout(file_path);
		opiped raii(fout.rdbuf(), cout);

		//linear_system_solution(N1);
		linear_system_solution(n);
		//linear_system_solution(N3);
		//matrix_test();
		system("pause");
		return 0;
	}
	catch (std::runtime_error const &ex)
	{
		std::cerr << "Error: " << ex.what() << std::endl;
		return 1;
	}
}

/* Lapack and Matirces eq sys solving*/
void solution()
{
	/* Locals */
	const lapack_int n = N1, nrhs = 1;

	double A1[n*n];
	double **A1_ = new double*[n];
	for (size_t i = 0; i < n; ++i)
	{
		A1_[i] = new double[n];
	}

	for (size_t i = 1; i <= n; ++i)
	{
		for (size_t j = 1; j <= n; ++j)
		{
			if (i == j)
			{
				A1[(i - 1)*n + (j - 1)] = (double)1 / j;
				A1_[i - 1][j - 1] = (double)1 / j;
			}
			else
			{
				A1[(i - 1)*n + (j - 1)] = (double)j * ((j % 2) ? -1 : 1);
				A1_[i - 1][j - 1] = (double)j * ((j % 2) ? -1 : 1);
			}

		}
	}

	double B1[n];
	double X[n];
	B1[0] = n;
	X[0] = 1;
	for (size_t i = 2; i <= n; ++i)
	{
		B1[(i - 1)] = 1;
		X[(i - 1)] = 1;
	}

	lapack_int  info, info_det;
	lapack_int ipiv[n], ipiv_det[n];

	/* Print Entry Matrix */
	print_matrix("Entry Matrix A1", n, n, A1, n);
	
	/* Print Right Rand Side */
	print_matrix("Right B1", n, nrhs, B1, nrhs);

	printf("\n");


	/* Solve the equations A1*X = B1 */
	info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, A1, n, ipiv,
		B1, nrhs);

	info_det = LAPACKE_dgetrf(n, n, n, A1, n, ipiv_det);

	/* Print solution A1*X = B1*/
	print_matrix("Solution A1*X = B1", n, nrhs, B1, nrhs);
	
	/* Print pivot indices */
	print_int_vector("Pivot indices A1", n, ipiv);
	print_int_vector("Pivot indices A1 det", n, ipiv_det);

	//delete res

	for (size_t i = 0; i < n; ++i)
	{
		delete[] A1_[i];
	}
	delete[] A1_;
}

void matrix_test()
{
	size_t n = 5, m = 5;
	Matrix mat1(n, n);
	Matrix mat2(n, n);
	Matrix mat3(n, n);
	for (size_t i = 0; i < n; ++i)
	{
		for (size_t j = 0; j < m; ++j)
		{
			mat1[i][j] = 2;
			mat2[i][j] = i;
			mat3[i][j] = i + j;
		}
	}

	mat1.print("mat1");
	mat2.print("mat2");
	
	mat2.transpose();
	mat2.print("transpose mat2:");
	mat3.print("mat3:");
}

void linear_system_test()
{
	const size_t n = 3;

	Matrix A("A.txt");
	Matrix b("b.txt");
	//A.print("A");
	//b.print("b");
	LinearSysterm lin1(A, b);

	lin1.Jacobi(max_iter, eps).print("Jacoby result");
	lin1.Zeidel(max_iter, eps).print("Zeidel result");

	/* LAPACK Solution*/
	lapack_int  info;
	lapack_int ipiv[n];
	double *lap_A = convert_to_lapack_matrix(A);
	double *lap_b = convert_to_lapack_matrix(b);

	/* Print Entry Matrix */
	//print_matrix("Entry LAPACK Matrix A", n, n, lap_A, n);
	
	/* Print Right Rand Side */
	//print_matrix("Right LAPACK b", n, 1, lap_b, 1);

	/* Solve the equations A*X = b */
	info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, lap_A, n, ipiv,
		lap_b, 1);
		
	/* Print solution A1*X = B1*/
	print_matrix("LAPACK Solution", n, 1, lap_b, 1);

	delete [] lap_b;
	delete [] lap_A;
}

void linear_system_solution(size_t const n)
{
	double *A_lapack = new double[n*n];

	for (size_t i = 1; i <= n; ++i)
	{
		for (size_t j = 1; j <= n; ++j)
		{
			if (i == j)
			{
				A_lapack[(i - 1)*n + (j - 1)] = (double)1 / j;				
			}
			else
			{
				A_lapack[(i - 1)*n + (j - 1)] = (double)j * ((j % 2) ? -1 : 1);				
			}
		}
	}

	double *b_lapack = new double[n];
	double *X = new double[n];

	b_lapack[0] = n;	
	for (size_t i = 2; i <= n; ++i)
	{
		b_lapack[(i - 1)] = 1;
	}

	Matrix A(n, n, A_lapack);
	Matrix b(n, 1, b_lapack);


	cout << "Solutios:" << endl << endl;
	cout << "N = " << n << endl << endl;
	A.print("A");
	b.print("b");
	
	A = A.transpose() * A;
	b = A.transpose() * b;	

	A.print("AT * A");
	b.print("AT * b");

	delete[] A_lapack;
	delete[] b_lapack;
	A_lapack = convert_to_lapack_matrix(A);
	b_lapack = convert_to_lapack_matrix(b);

	LinearSysterm lin1(A, b);

	clock_t time;
	time = clock();

	Matrix JacSol = lin1.Jacobi(max_iter, eps);
	JacSol.print("Jacoby result");	
	cout << "Time of solving: ";	
	cout << (double)(clock() - time) / CLOCKS_PER_SEC << " sec." << endl;
	cout << endl << endl;

	printf("Time of solving:\n");
	printf("%8.6f sec\n\n\n", (double)(clock() - time) / CLOCKS_PER_SEC);
		
	time = clock();
	Matrix  ZeiSol = lin1.Zeidel(max_iter, eps);
	ZeiSol.print("Zeidel result");
	cout << "Time of solving: ";
	cout << (double)(clock() - time) / CLOCKS_PER_SEC << " sec." << endl;
	cout << endl << endl;

	printf("Time of solving:\n");
	printf("%8.6f sec\n\n\n", (double)(clock() - time) / CLOCKS_PER_SEC);

	time = clock();
	Matrix RelaxSol05 = lin1.Relax(0.5, max_iter, eps);
	RelaxSol05.print("mu = 0.5 result");
	cout << "Time of solving: ";
	cout << (double)(clock() - time) / CLOCKS_PER_SEC << " sec." << endl;
	cout << endl << endl;

	
	printf("Time of solving:\n");
	printf("%8.6f sec\n\n\n", (double)(clock() - time) / CLOCKS_PER_SEC);
	
	time = clock();
	Matrix RelaxSol15 = lin1.Relax(1.5, max_iter, eps);
	RelaxSol15.print("Relax with mu = 1.5 result");
	cout << "Time of solving: ";
	cout << (double)(clock() - time) / CLOCKS_PER_SEC << " sec." << endl;
	cout << endl << endl;

	
	printf("Time of solving:\n");
	printf("%8.6f sec\n\n\n", (double)(clock() - time) / CLOCKS_PER_SEC);

	time = clock();
	/*lin1.Relax(2.5, max_iter, eps).print("Relax with mu = 2.5 result");
	cout << "Time of solving: ";
	cout << (double)(clock() - time) / CLOCKS_PER_SEC << " sec." << endl;
	cout << endl << endl;
	time = clock();*/

	/* LAPACK Solution*/
	lapack_int  info;
	lapack_int *ipiv = new lapack_int[n];
	
	
	/* Print Entry Matrix */
	//print_matrix("Entry LAPACK Matrix A", n, n, lap_A, n);

	/* Print Right Rand Side */
	//print_matrix("Right LAPACK b", n, 1, lap_b, 1);

	/* Solve the equations A*X = b */
	info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, A_lapack, n, ipiv,
		b_lapack, 1);
	
	if (info > 0) {
		printf("The diagonal element of the triangular factor of A,\n");
		printf("U(%i,%i) is zero, so that A is singular;\n", info, info);
		printf("the solution could not be computed.\n");
		exit(1);
	}
	if (info <0) exit(1);

	/*Compute the determinant det = det(A)
	if ipiv[i] > 0, then D(i,i) is a 1x1 block diagonal
	if ipiv[i] = ipiv[i-1] < 0, then D(i-1,i-1),
	D(i-1,i), and D(i,i) form a 2x2 block diagonal*/
	/*double *D = A_lapack;
	double det = 1.0;
	for (size_t i = 0; i < n; i++) {
		if (ipiv[i] > 0) {
			det *= D[i, i];
		}
		else if (i > 0 && ipiv[i] < 0 && ipiv[i - 1] == ipiv[i]) {
			det *= D[i, i] * D[i - 1, i - 1] - D[i - 1, i] * D[i - 1, i];
		}
	}*/

	//cout << endl << "Det A = " << det << endl << endl;
	/* Print details of LU factorization */
	//print_matrix("Details of LU factorization", n, n, A_lapack, n);
		
	/* Print details det solution */
	//print_matrix("Details of det solution", n, n, A_lapack, n);
	
	/* Check for the exact singularity */
	

	/* Print solution A1*X = B1*/
	cout << "LAPACK dgesv method solution:" << endl;
	print_matrix("LAPACK Solution", n, 1, b_lapack, 1);
	cout << "Time of solving: ";
	cout << (double)(clock() - time) / CLOCKS_PER_SEC << " sec." << endl;
	cout << endl << endl;

	printf("LAPACK dgesv:\n");
	printf("Time of solving:\n");
	printf("%8.6f sec\n\n\n", (double)(clock() - time) / CLOCKS_PER_SEC);

	time = clock();
	
	/*print_int_vector("Pivot indices:", n, ipiv);
	cout << endl << endl;*/

	
	//-------------------------------------------------------------------------------------------
	
	LAPACKE_dgetri(LAPACK_ROW_MAJOR, n, A_lapack, n, ipiv);
	
	A.print("Original A");
	Matrix invA(n, n, A_lapack);
	invA.print("Inverse A");
	(A*invA).print("A * inverse_of_A:");
	//-------------------------------------------------------------------------------------------

	char	jobvl = 'V';
	char	jobvr = 'V';

	int		n_ = n;
	double *wr = new double[n];
	double *wi = new double[n];

	double *vl = new double[n*n];
	int		ldvl = n;
	double *vr = new double[n*n];
	int		ldvr = n;

	LAPACKE_dgeev(LAPACK_ROW_MAJOR, jobvl, jobvr, n, A_lapack, n, wr, wi, vl, ldvl, vr, ldvr);

	size_t max_evi = wr[0];
	size_t min_evi = wr[0];
	
	if (info == 0)
	{
		int i;
		cout << "The eigenvalues are:" << endl;
		for (i = 0; i < n; ++i)
		{
			if (wr[i] > wr[max_evi])
			{
				max_evi = i;
			}

			if (wr[i] < wr[min_evi])
			{
				min_evi = i;
			}

			cout.setf(ios::fixed);
			cout.width(8);
			cout.precision(2);
			cout << wr[i];
			if (wi[i])
			{
				cout << " + i" << wi[i];
			}
			cout << endl;		
		}
	}

	cout << endl << endl;
	cout << "Min eigenvalue = " << wr[min_evi] << endl << endl;
	printf("Min eigenvalue = %10.10f\n", wr[min_evi]);
	
	print_double_vector("Min eigen vector:", n, vr + n*min_evi);

	cout << "Max eigenvalue = " << wr[max_evi] << endl << endl;
	printf("\nMax eigenvalue = %10.10f\n\n", wr[max_evi]);

	print_double_vector("Min eigen vector:", n, vr + n*max_evi);



	delete[] b_lapack;
	delete[] A_lapack;
	delete[] X;
	delete[] ipiv;

	delete[] wr;
	delete[] wi;
	delete[] vl;
	delete[] vr;



}

